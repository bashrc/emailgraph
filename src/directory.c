#include "emailgraph.h"

/* This is a modified version of the recursive directory
   reading function from:
   http://rosettacode.org/wiki/Walk_Directory_Tree#Library:_POSIX
   Licensed under GNU FDL
*/
int walk_recur(char *dname, regex_t *reg, int spec, filename_list * filenames)
{
    filename_list * f;
    struct filename_list *tmp;
    struct dirent *dent;
    DIR *dir;
    struct stat st;
    char fn[FILENAME_MAX];
    int res = WALK_OK;
    int len = strlen(dname);
    if (len >= FILENAME_MAX - 1)
        return WALK_NAMETOOLONG;

    strcpy(fn, dname);
    fn[len++] = '/';

    if (!(dir = opendir(dname))) {
        return WALK_BADIO;
    }

    errno = 0;
    while ((dent = readdir(dir))) {
        if (!(spec & WS_DOTFILES) && dent->d_name[0] == '.')
            continue;
        if (!strcmp(dent->d_name, ".") || !strcmp(dent->d_name, ".."))
            continue;

        strncpy(fn + len, dent->d_name, FILENAME_MAX - len);
        if (lstat(fn, &st) == -1) {
            warn("Can't stat %s", fn);
            res = WALK_BADIO;
            continue;
        }

        /* don't follow symlink unless told so */
        if (S_ISLNK(st.st_mode) && !(spec & WS_FOLLOWLINK))
            continue;

        /* will be false for symlinked dirs */
        if (S_ISDIR(st.st_mode)) {
            /* recursively follow dirs */
            if ((spec & WS_RECURSIVE))
                walk_recur(fn, reg, spec, filenames);

            if (!(spec & WS_MATCHDIRS)) continue;
        }
        else {
            /* pattern match */
            /*if (!regexec(reg, fn, 0, 0, 0)) {*/
            f = (filename_list*)malloc(sizeof(filename_list));
            tmp = filenames->next;
            sprintf((char*)(f->filename),"%s",fn);
            f->next = tmp;
            filenames->next=(struct filename_list*)f;
            /*}*/
        }
    }

    if (dir) closedir(dir);
    return res ? res : errno ? WALK_BADIO : WALK_OK;
}

int walk_dir(char *dname, char *pattern, int spec, filename_list * filenames)
{
    regex_t r;
    int res;

    if (regcomp(&r, pattern, REG_EXTENDED | REG_NOSUB))
        return WALK_BADPATTERN;
    res = walk_recur(dname, &r, spec, filenames);
    regfree(&r);

    return res;
}
