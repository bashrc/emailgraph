/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

void free_events(contact * c)
{
    contact_event * next_event = (contact_event*)(c->events);
    contact_event * tmp;
    while (next_event!=NULL) {
        tmp = next_event;
        next_event = (contact_event*)(next_event->next);
        free((void*)tmp);
    }
    c->events=NULL;
}

/* remove a contact event */
void remove_contact_event(contact_event * event)
{
    contact_event * next = (contact_event*)(event->next);
    contact_event * previous = (contact_event*)(event->previous);

    if (next!=NULL) next->previous = (struct contact_event*)previous;
    if (previous!=NULL) previous->next = (struct contact_event*)next;
    free((void*)event);
}

/* add an event to the given base contact */
void add_contact_event(contact * base, contact* from, contact * to, struct tm contact_date)
{
    contact_event * events;
    contact_event * new_event = (contact_event*)malloc(sizeof(contact_event));
    if (new_event!=NULL) {
        new_event->from = (struct contact*)from;
        new_event->to = (struct contact*)to;
        new_event->date = contact_date;
        new_event->next = (struct contact_event*)(base->events);
        new_event->previous=NULL;
        events = (contact_event*)(base->events);
        if (events!=NULL) {
            events->previous=(struct contact_event*)new_event;
        }
        base->events=(struct contact_event*)new_event;
    }
    else {
        fprintf(stderr,"add_contact_event: unable to allocate memory\n");
    }
}

/* Adds events */
void add_contact_events(
    contact ** fromcontacts,
    contact ** tocontacts,
    struct tm contact_date)
{
    int i=0,j;

    while (fromcontacts[i]!=NULL) {
        j=0;
        while (tocontacts[j]!=NULL) {
            add_contact_event(fromcontacts[i],fromcontacts[i],tocontacts[j],contact_date);
            add_contact_event(tocontacts[j],fromcontacts[i],tocontacts[j],contact_date);
            j++;
        }
        i++;
    }
}

/* Does the given contact contain the given event? */
int contains_event(contact * c, contact * from, contact * to, struct tm contact_date)
{
    contact_event * next_event = (contact_event*)c->events;
    while (next_event!=NULL) {
        if ((next_event->from==(struct contact*)from) && (next_event->to==(struct contact*)to) &&
                (next_event->date.tm_year==contact_date.tm_year) &&
                (next_event->date.tm_mday==contact_date.tm_mday) &&
                (next_event->date.tm_mon==contact_date.tm_mon)) {
            return 1;
        }
        next_event = (contact_event*)next_event->next;
    }
    return 0;
}

/* old contact is replaced by new contact */
void merge_events(contact * new_contact, contact * old_contact)
{
    contact_event * next_event = (contact_event*)(old_contact->events);
    while (next_event!=NULL) {
        if (next_event->from==(struct contact*)old_contact) {
            if (contains_event(new_contact, new_contact, (contact*)(next_event->to), next_event->date)==0) {
                add_contact_event(new_contact, new_contact, (contact*)(next_event->to), next_event->date);
            }
        }
        else {
            if (contains_event(new_contact, (contact*)(next_event->from), new_contact, next_event->date)==0) {
                add_contact_event(new_contact, (contact*)(next_event->from), new_contact, next_event->date);
            }
        }
        next_event = (contact_event*)(next_event->next);
    }
    free_events(old_contact);
}
