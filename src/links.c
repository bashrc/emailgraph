/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"


void free_links(contact * c)
{
    contact_link * next_link = (contact_link*)(c->links);
    contact_link * tmp;
    while (next_link!=NULL) {
        tmp = next_link;
        next_link = (contact_link*)(next_link->next);
        free((void*)tmp);
    }
    c->links=NULL;
}

void remove_links_base(contact * base, contact * old_contact)
{
    contact_link * next_link = (contact_link*)(base->links);
    contact * from, * to;
    contact_link * prev_link = NULL, * tmp;
    contact_link * new_base_link = NULL;

    while (next_link!=NULL) {
        from = (contact*)(next_link->from);
        to = (contact*)(next_link->to);
        if ((from == old_contact) || (to == old_contact)) {
            tmp = (contact_link*)(next_link->next);
            free((void*)next_link);
            next_link=tmp;
        }
        else {
            if (new_base_link==NULL) {
                new_base_link = next_link;
            }
            next_link->previous=(struct contact_link*)prev_link;
            if (prev_link!=NULL) {
                prev_link->next=(struct contact_link*)next_link;
            }
            prev_link = next_link;
            next_link = (contact_link*)(next_link->next);
        }
    }
    base->links = (struct contact_link*)new_base_link;
    if (prev_link!=NULL) prev_link->next=NULL;
}

void remove_links(contact * c)
{
    contact_link * next_link = (contact_link*)(c->links);
    contact * other;
    while (next_link!=NULL) {
        if ((contact*)(next_link->from) == c) {
            other=(contact*)(next_link->to);
            if (other!=c) {
                remove_links_base(other,c);
            }
        }
        if ((contact*)(next_link->to) == c) {
            other=(contact*)(next_link->from);
            if (other!=c) {
                remove_links_base(other,c);
            }
        }
        next_link = (contact_link*)(next_link->next);
    }
}

/* remove a contact link */
void remove_contact_link(contact_link * link)
{
    contact_link * next = (contact_link*)(link->next);
    contact_link * previous = (contact_link*)(link->previous);

    if (next!=NULL) next->previous = (struct contact_link*)previous;
    if (previous!=NULL) previous->next = (struct contact_link*)next;
    free((void*)link);
}

/* Does the given contact contain the given link? */
contact_link * contains_link(contact * c, contact * from, contact * to)
{
    contact_link * next_link;

    next_link  = (contact_link*)(c->links);
    while (next_link!=NULL) {
        if ((next_link->from==(struct contact*)from) && (next_link->to==(struct contact*)to)) {
            return next_link;
        }
        next_link = (contact_link*)next_link->next;
    }
    return NULL;
}

/* add a link to the given base contact */
void add_contact_link(contact * base, contact* from, contact * to)
{
    contact_link * links;
    contact_link * new_link, * lnk;

    if (from!=to) {
        lnk = contains_link(base,from,to);
        if (lnk!=NULL) {
            lnk->hits++;
        }
        else {
            new_link = (contact_link*)malloc(sizeof(contact_link));
            if (new_link!=NULL) {
                new_link->from = (struct contact*)from;
                new_link->to = (struct contact*)to;
                new_link->hits=1;
                new_link->next = (struct contact_link*)(base->links);
                new_link->previous=NULL;
                links = (contact_link*)(base->links);
                if (links!=NULL) {
                    links->previous=(struct contact_link*)new_link;
                }
                base->links=(struct contact_link*)new_link;
            }
            else {
                fprintf(stderr,"add_contact_link: unable to allocate memory\n");
            }
        }
    }
}

/* Adds links */
int add_contact_links(
    contact ** fromcontacts,
    contact ** tocontacts,
    contact ** cccontacts,
    int gender)
{
    int i=0,j,add;
    int added=0;

    /* from -> to */
    while (fromcontacts[i]!=NULL) {
        j=0;
        while (tocontacts[j]!=NULL) {
            add=1;
            if (gender==GENDER_MALE_FEMALE) {
                if ((fromcontacts[i]->gender == tocontacts[j]->gender) ||
                        (fromcontacts[i]->gender == GENDER_UNKNOWN) ||
                        (tocontacts[j]->gender == GENDER_UNKNOWN)) {
                    add=0;
                }
            }
            if (add!=0) {
                add_contact_link(fromcontacts[i],fromcontacts[i],tocontacts[j]);
                add_contact_link(tocontacts[j],fromcontacts[i],tocontacts[j]);
                added=1;
            }
            j++;
        }
        i++;
    }

    /* from -> cc */
    i=0;
    while (fromcontacts[i]!=NULL) {
        j=0;
        while (cccontacts[j]!=NULL) {
            add=1;
            if (gender==GENDER_MALE_FEMALE) {
                if ((fromcontacts[i]->gender == cccontacts[j]->gender) ||
                        (fromcontacts[i]->gender == GENDER_UNKNOWN) ||
                        (cccontacts[j]->gender == GENDER_UNKNOWN)) {
                    add=0;
                }
            }
            if (add!=0) {
                add_contact_link(fromcontacts[i],fromcontacts[i],cccontacts[j]);
                add_contact_link(cccontacts[j],fromcontacts[i],cccontacts[j]);
                added=1;
            }
            j++;
        }
        i++;
    }
    return added;
}

void consolidate_contact_links(contact * c)
{
    int merges=0;
    contact_link * lnk, * prev, * tmp;
    contact_link * next_link, * new_links;

    if (c!=NULL) {

        /* mark merged links */
        next_link = (contact_link*)(c->links);
        while (next_link!=NULL) {
            if (next_link->from!=NULL) {
                lnk = (contact_link*)(next_link->next);
                while (lnk!=NULL) {
                    if (lnk->from!=NULL) {
                        if ((next_link->from==lnk->from) && (next_link->to==lnk->to)) {
                            next_link->hits += lnk->hits;
                            lnk->from=NULL;
                            lnk->to=NULL;
                            merges++;
                        }
                    }
                    lnk = (contact_link*)(lnk->next);
                }
            }
            next_link = (contact_link*)(next_link->next);
        }

        /* create the new list */
        if (merges>0) {
            prev=NULL;
            next_link = (contact_link*)(c->links);
            new_links=NULL;
            while (next_link!=NULL) {
                if (next_link->from!=NULL) {
                    next_link->previous = (struct contact_link*)prev;
                    if (prev!=NULL) {
                        prev->next = (struct contact_link*)next_link;
                    }
                    else {
                        new_links=next_link;
                    }
                    prev=next_link;
                    next_link = (contact_link*)(next_link->next);
                }
                else {
                    tmp = (contact_link*)(next_link->next);
                    free(next_link);
                    next_link=tmp;
                }
            }
            if (prev!=NULL) prev->next=NULL;
            c->links=(struct contact_link*)new_links;
        }
    }
}

void replace_link_contact(contact * base, contact * old_contact, contact * new_contact)
{
    contact_link * next_link = NULL;
    contact_link * lnk;
    int replaced=0;

    if (base != NULL) {
        next_link = (contact_link*)(base->links);
        while (next_link!=NULL) {
            if (next_link->from==(struct contact*)old_contact) {
                replaced=1;
                next_link->from=(struct contact*)new_contact;
                lnk = contains_link(new_contact, new_contact, (contact*)(next_link->to));
                if (lnk==NULL) {
                    add_contact_link(new_contact, new_contact, (contact*)(next_link->to));
                }
            }
            if (next_link->to==(struct contact*)old_contact) {
                replaced=1;
                next_link->to=(struct contact*)new_contact;
                lnk = contains_link(new_contact, (contact*)(next_link->from), new_contact);
                if (lnk==NULL) {
                    add_contact_link(new_contact, (contact*)(next_link->from), new_contact);
                }
            }
            next_link = (contact_link*)(next_link->next);
        }
    }
    if (replaced!=0) {
        consolidate_contact_links(base);
    }
}

/* old contact is replaced by new contact */
void merge_links(contact * new_contact, contact * old_contact)
{
    contact_link * next_link;
    contact * base;

    next_link = (contact_link*)(old_contact->links);
    while (next_link!=NULL) {
        /* from another contact to the old one */
        if (next_link->to == (struct contact*)old_contact) {
            base = (contact*)(next_link->from);
            replace_link_contact(base,old_contact,new_contact);
        }
        /* from the old contact to another */
        if (next_link->from == (struct contact*)old_contact) {
            base = (contact*)(next_link->to);
            replace_link_contact(base,old_contact,new_contact);
        }
        next_link = (contact_link*)(next_link->next);
    }

    replace_link_contact(new_contact,old_contact,new_contact);

    consolidate_contact_links(new_contact);

    free_links(old_contact);
}

void consolidate_links(contact * contact_list)
{
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        consolidate_contact_links(next_contact);
        next_contact = (contact*)(next_contact->next);
    }
}
