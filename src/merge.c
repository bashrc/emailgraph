/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

void merge_read_file(char * filename, merge_rule * rules)
{
    FILE * fp;
    char linestr[256];
    int i,n;
    char merge_from[256];
    char merge_to[256];
    merge_rule * new_rule;
    char * retval;

    rules->next=NULL;
    rules->fromtext[0]='\0';
    rules->totext[0]='\0';

    fp = fopen(filename,"r");
    if (fp!=NULL) {
        while (!feof(fp)) {
            retval = fgets(linestr,255,fp);
            if (retval != NULL) {
                Trim(linestr,' ');
                /* comment */
                if (strlen(linestr)>0) {
                    if ((linestr[0]=='#') ||
                            (linestr[0]=='/')) {
                        continue;
                    }
                }
                if (ContainsChar(linestr,',')!=0) {

                    i=0;
                    while ((i<strlen(linestr)-1) && (linestr[i]!=',')) {
                        merge_from[i]=linestr[i];
                        i++;
                    }
                    merge_from[i]='\0';
                    Trim(merge_from,' ');

                    if (strlen(merge_from)>0) {

                        i++;
                        n=0;
                        while (i<strlen(linestr)) {
                            if ((linestr[i]!=13) && (linestr[i]!=10)) {
                                merge_to[n++]=linestr[i];
                            }
                            i++;
                        }
                        merge_to[n]='\0';
                        Trim(merge_to,' ');
                        Trim(merge_to,',');

                        if (strlen(merge_to)>0) {
                            if (rules->fromtext[0]=='\0') {
                                sprintf((char*)rules->fromtext,"%s",merge_from);
                                sprintf((char*)rules->totext,"%s",merge_to);
                            }
                            else {
                                new_rule = (merge_rule*)malloc(sizeof(merge_rule));
                                if (new_rule!=NULL) {
                                    sprintf((char*)new_rule->fromtext,"%s",merge_from);
                                    sprintf((char*)new_rule->totext,"%s",merge_to);
                                    new_rule->next = (struct merge_rule*)(rules->next);
                                    rules->next=(struct merge_rule*)new_rule;
                                }
                                else {
                                    fprintf(stderr,"Unable to allocate memory for merge rule\n");
                                }
                            }
                        }
                    }
                }
            }
        }
        fclose(fp);
    }
}

void merge_free(merge_rule * rules)
{
    merge_rule * next_rule = rules;
    merge_rule * tmp;

    while (next_rule!=NULL) {
        tmp = next_rule;
        next_rule=(merge_rule*)(next_rule->next);
        free((void*)tmp);
    }
}

void merge_contacts_with_rules(contact * contact_list, merge_rule * rules)
{
    merge_rule * next_rule;
    contact * c, * c2;
    contact * example = (contact*)malloc(sizeof(contact));

    next_rule = rules;
    while (next_rule!=NULL) {
        if (next_rule->fromtext[0]!='\0') {
            if (ContainsChar(next_rule->fromtext,'@')==0) {
                sprintf(example->name,"%s",next_rule->fromtext);
                c = find_contact(example,contact_list);
                if (c!=NULL) {
                    sprintf(example->name,"%s",next_rule->totext);
                    c2 = find_contact(example,contact_list);
                    if (c2!=NULL) {
                        merge_contacts(c2,c,contact_list);
                    }
                    else {
                        sprintf(c->name,"%s",next_rule->totext);
                    }
                }
            }
            else {
                sprintf(example->email,"%s",next_rule->fromtext);
                c = find_contact_email(example,contact_list);
                if (c!=NULL) {
                    sprintf(example->email,"%s",next_rule->totext);
                    c2 = find_contact_email(example,contact_list);
                    if (c2!=NULL) {
                        merge_contacts(c2,c,contact_list);
                    }
                }
            }
        }
        next_rule = (merge_rule*)(next_rule->next);
    }
    free(example);
}
