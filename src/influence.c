/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* spreads influence from the current contact */
unsigned int spread_contact_influence(contact * c)
{
    unsigned int updates=0;
    contact_link * next_link;
    contact * c2;

    next_link = (contact_link*)(c->links);
    while (next_link!=NULL) {
        if ((contact*)(next_link->from) == c) {
            c2 = (contact*)(next_link->to);
            if (c2!=NULL) {
                if (c2->selected == 0) {
                    c2->selected=(char)2;
                    updates++;
                }
            }
        }
        next_link = (contact_link*)(next_link->next);
    }
    return updates;
}

unsigned int contact_influence(contact * c, contact * contact_list)
{
    contact * next_contact;
    unsigned int updates=1,v,influence=0,level=1000,itt=0;

    clear_selected_flags(contact_list);

    c->selected=(char)1;

    while ((updates>0) && (itt<10)) {
        updates=0;
        itt++;

        next_contact = (contact*)(contact_list->next);
        while (next_contact!=NULL) {
            if (next_contact->selected==(char)1) {
                v = spread_contact_influence(next_contact);
                updates += v;
                influence += v*level;
            }
            next_contact = (contact*)(next_contact->next);
        }

        if (updates>0) {
            next_contact = (contact*)(contact_list->next);
            while (next_contact!=NULL) {
                if (next_contact->selected==(char)2) {
                    next_contact->selected=(char)1;
                }
                next_contact = (contact*)(next_contact->next);
            }
        }

        level/=2;
        if (level<1) level=1;
    }

    return influence;
}

void update_influence(contact * contact_list)
{
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        next_contact->influence = contact_influence(next_contact,contact_list);
        next_contact = (contact*)(next_contact->next);
    }
}
