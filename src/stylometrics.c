/*
    Stylometrics
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

#ifdef STYLOMETRICS
stylometrics style_buffer;
#endif
char word_buffer[256];

/* clears the stylometrics buffer */
void clear_style()
{
#ifdef STYLOMETRICS
    int i;
    style_buffer.sentence_length=0;
    style_buffer.sentences=0;
    style_buffer.word_length=0;
    style_buffer.words=0;
    style_buffer.emails=1;
    for (i=0; i<8; i++) {
        style_buffer.punctuation[i]=0;
    }
#endif
}

/* Sets metrics for the given contact */
void set_style(contact * c)
{
#ifdef STYLOMETRICS
    int i;
    c->style.sentence_length = style_buffer.sentence_length;
    c->style.sentences = style_buffer.sentences;
    c->style.word_length = style_buffer.word_length;
    c->style.words = style_buffer.words;
    c->style.emails = style_buffer.emails;
    for (i=0; i<8; i++) {
        c->style.punctuation[i] = style_buffer.punctuation[i];
    }
#endif
}

/* Merges metrics from another contact */
void update_style(contact * c, contact * other)
{
#ifdef STYLOMETRICS
    int i;
    c->style.sentence_length += other->style.sentence_length;
    c->style.sentences += other->style.sentences;
    c->style.word_length += other->style.word_length;
    c->style.words += other->style.words;
    c->style.emails += other->style.emails;
    for (i=0; i<8; i++) {
        c->style.punctuation[i] += other->style.punctuation[i];
    }
#endif
}

/* Processing sentences ------------------------------------------------------------------*/

/* Processes an individual word */
void stylometrics_process_word(char * word, int * unused)
{
#ifdef STYLOMETRICS
    style_buffer.word_length+=(unsigned int)strlen(word);
    style_buffer.words++;
#endif
}

/* Processes a sentence */
int stylometrics_process_sentence(char * sentence, int * unused)
{
    int no_of_words=0;

    if (sentence[0]!='\0') {
        no_of_words = detect_words(
                          sentence,
                          word_buffer,
                          unused,
                          stylometrics_process_word);
        if (no_of_words>0) {
#ifdef STYLOMETRICS
            int i;
            style_buffer.sentence_length += (unsigned int)no_of_words;
            style_buffer.sentences++;
            for (i=0; i<strlen(sentence); i++) {
                switch(sentence[i]) {
                case ' ': {
                    style_buffer.punctuation[0]++;
                    break;
                }
                case ';': {
                    style_buffer.punctuation[1]++;
                    break;
                }
                case '"': {
                    style_buffer.punctuation[2]++;
                    break;
                }
                case (char)39: {
                    style_buffer.punctuation[3]++;
                    break;
                }
                case '-': {
                    style_buffer.punctuation[3]++;
                    break;
                }
                case '?': {
                    style_buffer.punctuation[4]++;
                    break;
                }
                case '!': {
                    style_buffer.punctuation[5]++;
                    break;
                }
                case '$': {
                    style_buffer.punctuation[6]++;
                    break;
                }
                case (char)156: {
                    style_buffer.punctuation[6]++;
                    break;
                }
                case '(': {
                    style_buffer.punctuation[7]++;
                    break;
                }
                case ')': {
                    style_buffer.punctuation[7]++;
                    break;
                }
                }
            }
#endif
        }
    }
    return no_of_words;
}
