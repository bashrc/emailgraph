/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* Initialisation ---------------------------------------------------------------------*/

/* Initialise a contact */
void init_contact(contact * c)
{
#ifdef STYLOMETRICS
    int i;
    c->style.sentence_length=0;
    c->style.word_length=0;
    c->style.sentences=0;
    c->style.emails=0;
    c->style.words=0;
    for (i=0; i<8; i++) {
        c->style.punctuation[i]=0;
    }
#endif
    c->next=NULL;
    c->previous=NULL;
    c->events=NULL;
    c->links=NULL;
    c->hash=0;
    c->influence=0;
    c->ID=0;
    c->selected=0;
    c->gender=0;
    c->name[0]='\0';
    c->email[0]='\0';
    c->sent=0;
    c->received=0;
    c->valence=0;
}

/* Copying --------------------------------------------------------------------------*/

void copy_contact(contact * c, contact * other)
{
    c->hash=other->hash;
    c->influence=other->influence;
    c->ID=other->ID;
    c->selected=other->selected;
    c->gender=other->gender;
    sprintf((char*)(c->name),"%s",other->name);
    sprintf((char*)(c->email),"%s",other->email);
    c->sent=other->sent;
    c->received=other->received;
    c->valence=other->valence;
}

/* Selections ------------------------------------------------------------------------*/

/* clears the selected status for all contacts */
void clear_selected_flags(contact * contact_list)
{
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        next_contact->selected=(char)0;
        next_contact = (contact*)(next_contact->next);
    }
}

/* sorting ------ --------------------------------------------------------------------*/

/* compares two contacts and returns a similarity value */
int compare_contacts(contact * c1, contact * c2)
{
    return strcmp(c1->name,c2->name);
}

contact * min_contact(contact * c)
{
    unsigned int min=c->hash;
    contact * next_contact=NULL, * result=NULL;

    next_contact = (contact*)(c->next);
    while (next_contact!=NULL) {
        if (next_contact->hash < min) {
            min = next_contact->hash;
            result = next_contact;
        }
        next_contact = (contact*)(next_contact->next);
    }
    return result;
}

void insert_contact(contact * previous, contact * current, contact * next)
{
    if (previous!=NULL) previous->next = (struct contact*)current;
    if (next!=NULL) next->previous = (struct contact*)current;

    current->next = (struct contact*)next;
    current->previous = (struct contact*)previous;
}

void swap_contacts(contact * old_contact, contact * new_contact)
{
    contact * old_previous = (contact*)(old_contact->previous);
    contact * old_next = (contact*)(old_contact->next);
    contact * new_previous = (contact*)(new_contact->previous);
    contact * new_next = (contact*)(new_contact->next);

    insert_contact(old_previous, new_contact, old_next);
    insert_contact(new_previous, old_contact, new_next);

    if (old_next==new_contact) {
        new_contact->next = (struct contact*)old_contact;
        old_contact->previous = (struct contact*)new_contact;
    }
}

contact * sort_contacts(contact * contact_list, int sort_type)
{
    int i=0;
    contact * next_contact=NULL, * c=NULL;

    contact_list = update_hashes(contact_list,sort_type);

    /* sort */
    next_contact = contact_list;
    while (next_contact!=NULL) {
        c = min_contact(next_contact);
        if ((c!=NULL) && (c!=next_contact)) {
            swap_contacts(next_contact,c);
            if (next_contact==contact_list) {
                contact_list=c;
            }
            next_contact = c;
        }
        i++;
        next_contact = (contact*)(next_contact->next);
    }

    return contact_list;
}

/* Contacts -------------------------------------------------------------------------------*/

/* Checks whether the contact list contains certain contacts */
int contacts_contains(contact * search, contact * contact_list)
{
    int i=0;
    contact * next_contact=NULL, * next_contact2=NULL;

    next_contact = contact_list;
    while (next_contact!=NULL) {

        i=0;
        next_contact2 = search;
        while (next_contact2!=NULL) {
            if (next_contact==next_contact2) return i+1;
            i++;
            next_contact2 = (contact*)(next_contact2->next);
        }

        next_contact = (contact*)(next_contact->next);
    }
    return 0;
}

/* remove a contact from the list */
void remove_contact(contact * c)
{
    contact * c2=NULL;

    if (c->previous!=NULL) {
        c2 = (contact*)(c->previous);
        c2->next = c->next;
    }
    if (c->next!=NULL) {
        c2 = (contact*)(c->next);
        c2->previous = c->previous;
    }
    free_events(c);
    remove_links(c);
    free_links(c);
    free((void*)c);
}

/* removes contacts which have an email address as their name */
contact * remove_non_names(contact * contact_list)
{
    contact * tmp=NULL;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if ((ContainsChar(next_contact->name,'@')!=0) ||
                (ContainsChar(next_contact->name,'.')!=0) ||
                (ContainsChar(next_contact->name,'!')!=0)) {
            tmp = (contact*)(next_contact->next);
            if (next_contact==contact_list) contact_list=tmp;
            remove_contact(next_contact);
            next_contact = tmp;
        }
        else {
            next_contact = (contact*)(next_contact->next);
        }
    }
    return contact_list;
}

/* merge two contacts into one */
contact * merge_contacts(contact * c, contact * other, contact * contact_list)
{
    update_style(c,other);

    /* sum values */
    c->sent += other->sent;
    c->received += other->received;
    c->valence += other->valence;

    merge_events(c,other);
    merge_links(c,other);

    /* if we are removing the start of the contact list */
    if (other==contact_list) {
        contact_list = (contact*)(other->next);
    }

    /* Remove the merged contact */
    remove_contact(other);

    return contact_list;
}

contact * find_good_name(contact *contact_list, char * email, int no_at)
{
    contact * next_contact=NULL, * result=NULL;
    int score=0,best_score=0;

    next_contact = contact_list;
    while (next_contact!=NULL) {
        if (strcmp(next_contact->email,email)==0) {
            if ((no_at==0) || ((no_at!=0) && (ContainsChar(next_contact->name,'@')==0))) {
                score=1;
                if (ContainsChar(next_contact->name,'|')==0) score++;
                /* Favour names containing at least two parts */
                if (ContainsChar(next_contact->name,' ')!=0) score++;
                /* Favour names with two parts */
                if (CountChars(next_contact->name,' ')==1) score++;
                if (strlen(next_contact->name)>1) {
                    /* Favour names beginning with a capital letter */
                    if ((next_contact->name[0]>='A') && (next_contact->name[0]<='Z')) score++;
                    /* Don't favour single initial names */
                    if (next_contact->name[1]!=' ') score++;
                }
                /* Record the best scoring name */
                if (score>best_score) {
                    result = next_contact;
                    best_score=score;
                }
            }
        }
        next_contact = (contact*)(next_contact->next);
    }
    return result;
}

contact * merge_duplicates(contact *contact_list)
{
    contact * next_contact=NULL, * best=NULL, * temp=NULL;
    int i=0,merged=0;

    contact_list = update_hashes(contact_list,SORT_NAMES);

    /* i=0 First check if the name field contains an email address, then attempt to replace it with a real name
       i=1 Find duplicate names and try to replace them with the best scoring name  */
    for (i=0; i<2; i++) {
        next_contact = contact_list;
        while (next_contact!=NULL) {
            if (((i==0) && (ContainsChar(next_contact->name,'@')!=0)) || (i > 0)) {
                best = find_good_name(contact_list,next_contact->email,1-i);
                if ((best!=NULL) && (best!=next_contact)) {
                    temp = (contact*)(next_contact->next);
                    merged=1;
                    contact_list = merge_contacts(best,next_contact,contact_list);
                    next_contact = temp;
                }
            }
            if (merged==0) {
                next_contact = (contact*)(next_contact->next);
            }
            merged=0;
        }
    }
    return contact_list;
}

/* returns the contact with the given index */
contact * get_contact(contact * contact_list, int index)
{
    int i=0;
    contact * next_contact = contact_list;
    while ((next_contact!=NULL) && (i<index)) {
        i++;
        next_contact = (contact*)(next_contact->next);
    }
    return next_contact;
}

/* returns a pointer to a contact, or NULL if not found */
contact * find_contact(contact * c, contact * contact_list)
{
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if (compare_contacts(next_contact,c)==0) {
            return next_contact;
        }
        next_contact = (contact*)(next_contact->next);
    }
    return NULL;
}

/* returns a pointer to a contact, or NULL if not found */
contact * find_contact_email(contact * c, contact * contact_list)
{
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if (strcmp(next_contact->email,c->email)==0) {
            return next_contact;
        }
        next_contact = (contact*)(next_contact->next);
    }
    return NULL;
}

/* frees memory for a contact list */
void free_contacts(contact * contact_list)
{
    contact * next_contact = contact_list;
    contact * tmp=NULL;
    while (next_contact!=NULL) {
        free_events(next_contact);
        free_links(next_contact);
        tmp = next_contact;
        next_contact = (contact*)(next_contact->next);
        free((void*)tmp);
    }
}

/* adds a single contact to the list */
contact * add_contact(contact * c, contact * contact_list, int entry_type)
{
    contact * next_contact = find_contact(c,contact_list);
    contact * tmp=NULL, * new_contact=NULL;
    if (next_contact!=NULL) {
        if (entry_type!=ENTRY_FROM) {
            next_contact->received++;
        }
        else {
            next_contact->sent++;
            update_style(next_contact,c);
        }
        return next_contact;
    }
    else {
        if (contact_list->name[0]!='\0') {
            /* make a copy of the contact */
            new_contact = (contact*)malloc(sizeof(contact));
            if (new_contact!=NULL) {
                copy_contact(new_contact,c);
                new_contact->events=NULL;
                new_contact->links=NULL;
                new_contact->previous=NULL;

                tmp = (contact*)(contact_list->next);
                contact_list->next = (struct contact*)new_contact;
                new_contact->next = (struct contact*)tmp;
                new_contact->previous = (struct contact*)contact_list;
                if (tmp!=NULL) tmp->previous = (struct contact*)new_contact;
            }
            else {
                fprintf(stderr,"add_contact: unable to allocate memory\n");
            }
        }
        else {
            /* first entry */
            copy_contact(contact_list,c);
            contact_list->events=NULL;
            contact_list->links=NULL;
            contact_list->next=NULL;
            contact_list->previous=NULL;
            new_contact=contact_list;
        }

        if (new_contact!=NULL) {
            if (entry_type!=ENTRY_FROM) {
                new_contact->received=1;
                new_contact->sent=0;
            }
            else {
                new_contact->received=0;
                new_contact->sent=1;
                set_style(new_contact);
            }
        }

        return new_contact;
    }
}

/* adds list of contacts to the list */
void add_contacts(contact * contacts, contact * contact_list, int entry_type, contact ** result, int valence)
{
    int i=0;
    contact * c=NULL;

    contact * next_contact = contacts;
    while (next_contact!=NULL) {
        next_contact->valence=valence;
        c = add_contact(next_contact, contact_list, entry_type);
        if (c!=NULL) {
            result[i]=c;
            if (i<CONTACT_BUFFER_SIZE-2) i++;
        }
        next_contact = (contact*)(next_contact->next);
    }
    result[i]=NULL;
}
