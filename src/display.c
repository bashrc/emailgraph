/*
    Different ways of displaying email social graph data
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

void print_help()
{
    printf("\nNAME\n\n");
    printf("  emailgraph - plot social graphs and usage information from email logs\n\n");
    printf("OPTIONS\n\n");
    printf(" -m  --mbox <filename>      Mailbox filename\n");
    printf(" -d  --dir <directory>      Directory to search for emails\n");
    printf(" -c  --contacts             Print contacts\n");
    printf(" -t  --top                  Print the most active contacts\n");
    printf(" -i  --influence            Print contacts in order of influence\n");
    printf("     --valence              Print contacts in order of valence\n");
    printf("     --sentiment            Show valence in plots and social graphs\n");
    printf("     --sentimentneg         Show negative valence in plots and social graphs\n");
    printf("     --start <dd/mm/yyyy>   Start date\n");
    printf("     --end <dd/mm/yyyy>     End date\n");
    printf("     --starthour <hour>     Start time in hours (0-24)\n");
    printf("     --endhour <hour>       End time in hours (0-24)\n");
    printf("     --dot                  Print in dot file format\n");
    printf(" -w  --linewidth <width>    Maximum line width in dot file\n");
    printf("     --nonames              Don't use names within diagrams\n");
    printf("     --monthly              CSV containing monthly email volumes\n");
    printf("     --yearly               CSV containing yearly email volumes\n");
    printf("     --daily                CSV containing daily average email volume for each hour\n");
    printf("     --weekly               CSV containing weekly average email volume for each hour\n");
    printf(" -s  --sentences            Print sentences for subsequent semantic analysis\n");
    printf("     --plotdensity <image>  Plot email density to an image file\n");
    printf("     --plotdaily <image>    Plot daily email volumes to an image file\n");
    printf("     --plotweekly <image>   Plot weekly email volumes to an image file\n");
    printf("     --plotmonthly <image>  Plot monthly email volumes to an image file\n");
    printf("     --plotannual <image>   Plot annual average email volumes to an image file\n");
    printf("     --keywords <words>     Search for keywords\n");
    printf("     --corpus               Prints all emails which contain matching keywords\n");
    printf("     --title <title>        Title to be added to a plot\n");
    printf("     --min <number>         Minimum number of interactions\n");
    printf("     --gender <M/F/MF>      Select only males (M), females (F) or male/female interactions (MF)\n");
    printf("     --merge <filename>     Specify a merge file containing contacts to be merged\n");
    printf("     --exclude <filename>   Specify an exclude file containing contacts to be removed\n");
    printf("     --tests                Run unit tests\n");
    printf(" -v  --version              Show version number\n");
    printf(" -h  --help                 Show help\n");
    printf("\n");

    printf("EXAMPLE SOCIAL GRAPHS\n\n");
    printf("  To create a social graph in dot format for an entire email archive:\n\n");
    printf("      emailgraph -m mboxfile --dot > social_graph.dot\n\n");
    printf("  An email stream may also be piped in from elsewhere, like this:\n\n");
    printf("      cat mboxfile | emailgraph --dot > social_graph.dot\n\n");
    printf("  You can also process multiple emails within a directory structure.  This will\n");
    printf("  recursively examine subdirectories and attempt to read all files within them:\n\n");
    printf("      emailgraph -d emaildirectory --dot > social_graph.dot\n\n");
    printf("  To produce an anonymized social graph.  This is recommended if you are\n");
    printf("  going to publish the graphs publicly and wish the names of individuals\n");
    printf("  to remain private.\n\n");
    printf("      emailgraph -m mboxfile --dot --nonames > social_graph.dot\n\n");
    printf("  To limit between dates, where the date is given in dd/mm/yyyy format:\n\n");
    printf("      emailgraph -m mboxfile --dot --start 20/1/2007 --end 30/5/2009 > social_graph.dot\n\n");
    printf("  You can also specify a start date relative to the current date using the options:\n\n");
    printf("      --start month/3month/6month/year/2year/3year/4year/5year\n\n");
    printf("  Such as the following, to show a social graph for the previous 6 months:\n\n");
    printf("      emailgraph -m mboxfile --dot --start 6month > social_graph.dot\n\n");
    printf("  To limit between times of day (eg. within working hours):\n\n");
    printf("      emailgraph -m mboxfile --dot --starthour 9 --endhour 17 > social_graph.dot\n\n");
    printf("  To investigate graphs where participants are using particular keywords.\n");
    printf("  Keywords are not case sensitive and can also include names or email addresses.\n");
    printf("  Multiple keywords are separated by commas.\n\n");
    printf("      emailgraph -m mboxfile --dot --keywords %cfoo,bar%c > social_graph.dot\n\n",(char)34,(char)34);
    printf("  You can also use the --min option to show only those individuals with more than\n");
    printf("  a given number of interactions. The --corpus option may be used to print all emails\n");
    printf("  which contain matching keywords.\n\n");
    printf("      emailgraph -m mboxfile --keywords %cfoo,bar%c --corpus > emails.txt\n\n",(char)34,(char)34);
    printf("  Dot files may be viewed using other utilities, such as dotty, xdot or kgraphviewer.\n\n");

    printf("MERGING CONTACTS\n\n");
    printf("  In some cases you may see multiple nodes in the social graph which correspond to\n");
    printf("  the same person.  This may be because their name is spelled differently, or because\n");
    printf("  they have more then one email address.  To make the graph neater and easier to interpret\n");
    printf("  it is possible to manually specify the names or email addresses of individuals which\n");
    printf("  you wish to be merged together.  All entries are case sensitive.\n");
    printf("  To do this create a merge file such as the following:\n\n");
    printf("      J Smith,John Smith\n");
    printf("      Clarke K,Clarke Kent\n");
    printf("      Susan,Susan Jones\n\n");
    printf("  In each case the first name is the name as it currently appears in the graph and the second\n");
    printf("  is the name that you wish it to be changed to.  Then use the --merge option, for example:\n\n");
    printf("      emailgraph -m mboxfile --dot --merge mymerges.txt > social_graph.dot\n\n");

    printf("EXCLUDING CONTACTS\n\n");
    printf("  Contacts can also be excluded from the social graph.  This works in a similar manner to\n");
    printf("  merging, but uses the --exclude option to specify a file containing names to be removed.\n");
    printf("  Names are case sensitive.  For example you could make a file like this:\n\n");
    printf("      Kevin Killjoy\n");
    printf("      Mr Troll\n");
    printf("      Doctor Strangecode\n\n");
    printf("  Then call emailgraph as follows:\n\n");
    printf("      emailgraph -m mboxfile --dot --exclude banned.txt > social_graph.dot\n\n");

    printf("EXAMPLE USAGE GRAPHS\n\n");
    printf("  In order for this to work you must have gnuplot installed.\n");
    printf("  To plot email volume for each month, with a specified title:\n\n");
    printf("      emailgraph -m mboxfile --plotmonthly monthly_emails.png --title \"My graph title\"\n\n");
    printf("  To plot email volume for each day of the week:\n\n");
    printf("      emailgraph -m mboxfile --plotweekly weekly_emails.png\n\n");
    printf("  To plot email volume for each hour of the day:\n\n");
    printf("      emailgraph -m mboxfile --plotdaily daily_emails.png\n\n");
    printf("  To plot individual emails as points:\n\n");
    printf("      emailgraph -m mboxfile --plotdensity email_density.png\n\n");
    printf("  You can also combine this with keywords and date or time limits, as required.\n\n");

    printf("SENTIMENT ANALYSIS\n\n");
    printf("  Usage and social graphs may also be produced for measurements of valence.  For example\n");
    printf("  the following plots monthly total valence values:\n\n");
    printf("      emailgraph -m mboxfile --plotmonthly monthly.png --sentiment\n\n");
    printf("  If you are only interested in negative valence you can also do:\n\n");
    printf("      emailgraph -m mboxfile --plotmonthly monthly.png --sentimentneg\n\n");
    printf("  Also social graph nodes may have their background shading altered depending upon the\n");
    printf("  average valence values for emails sent by each individual:\n\n");
    printf("      emailgraph -m mboxfile --dot --sentiment > social_graph.dot\n\n");
    printf("  Here lighter background shades indicate more positive valence, and darker represent\n");
    printf("  lower or more negative valence values.\n\n");

    printf("BUGS\n\n");
    printf("  Report all bugs to https://bugs.launchpad.net/emailgraph\n\n");
}

/* print all contacts */
void print_contacts(contact * contact_list)
{
    contact * next_contact = contact_list;

#ifndef STYLOMETRICS
    printf("Name, Email, Received, Sent, Total, Influence\n");
    while (next_contact!=NULL) {
        printf("%s, %s, %d, %d, %d, %u\n",
               next_contact->name,next_contact->email,
               next_contact->received,next_contact->sent,next_contact->received+next_contact->sent,next_contact->influence);
        next_contact = (contact*)(next_contact->next);
    }
#else
    printf("Name, Email, Received, Sent, Total, Influence, Average Sentence Length (words), Average Word Length (chars), Average Sentences per Email, Average words per Email, Average Spaces per sentence, Average Semicolons per Sentence, Average Double Quotes per Sentence, Average Single Quotes per Sentence, Average Dashes per Sentence, Average Question Marks per Sentence, Average Exclaimation Marks per Sentence, Average $ per Sentence, Average Brackets per Sentence\n");
    while (next_contact!=NULL) {
        printf("%s, %s, %d, %d, %d, %u, ",
               next_contact->name,next_contact->email,
               next_contact->received,next_contact->sent,next_contact->received+next_contact->sent,next_contact->influence);
        if (next_contact->style.sentences>0) {
            printf("%.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f\n",
                   next_contact->style.sentence_length/(float)(next_contact->style.sentences),
                   next_contact->style.word_length/(float)(next_contact->style.words),
                   next_contact->style.sentences/(float)(next_contact->style.emails),
                   next_contact->style.words/(float)(next_contact->style.emails),
                   next_contact->style.punctuation[0]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[1]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[2]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[3]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[4]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[5]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[6]/(float)(next_contact->style.sentences),
                   next_contact->style.punctuation[7]/(float)(next_contact->style.sentences)
                  );
        }
        else {
            printf("0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0\n");
        }
        next_contact = (contact*)(next_contact->next);
    }
#endif
}

int file_exists(char * filename)
{
    FILE * fp = fopen(filename,"r");
    if (fp!=NULL) {
        fclose(fp);
        return 1;
    }
    return 0;
}

/* Updates the contact ID numbers */
void update_IDs(contact * contact_list)
{
    unsigned int ID=0;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        next_contact->ID=ID++;
        next_contact = (contact*)(next_contact->next);
    }
}

int interactions(contact * c)
{
    if (c->links!=NULL) {
        return (int)(c->sent+c->received);
    }
    return -1;
}

/* Emails for each hour of the day */
void print_volume_daily(int * day_histogram)
{
    int hr;

    printf("Time, Emails\n");
    for (hr=0; hr<24; hr++) {
        printf("%02d:00, %d\n",hr,day_histogram[hr]);
    }
}

int plot_volume_daily(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * day_histogram,
    int sentiment)
{
    char * plot_filename = "temp_plotfile.plot";
    char * data_filename = "temp_datafile.dat";
    char command[256];
    FILE * fp, * fp_data;
    int hr,retval=0;

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }
    if (file_exists(data_filename)!=0) {
        sprintf((char*)command,"rm %s",data_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fp_data = fopen(data_filename,"w");
        if (fp_data!=NULL) {
            for (hr=0; hr<24; hr++) {
                fprintf(fp_data,"%02d:00, %d\n",hr,day_histogram[hr]);
            }
            fclose(fp_data);
        }

        fprintf(fp,"reset\n");
        fprintf(fp,"set style histogram\n");
        fprintf(fp,"set title %c%s%c\n",(char)34,title,(char)34);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set mxtics\n");
        fprintf(fp,"set xtics border out scale 1,1 mirror rotate by 0  offset character 0, 0, 0 autofreq\n");
        fprintf(fp,"set xlabel %cTime%c\n",(char)34,(char)34);
        if (sentiment==0) {
            fprintf(fp,"set ylabel %cTotal Number of Emails%c\n",(char)34,(char)34);
        }
        else {
            fprintf(fp,"set ylabel %cValence%c\n",(char)34,(char)34);
        }
        fprintf(fp,"set grid\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set xdata time\n");
        fprintf(fp,"set timefmt %c%%H:%%M%c\n",(char)34,(char)34);
        fprintf(fp,"set format x %c%%H:%%M%c\n",(char)34,(char)34);
        fprintf(fp,"set terminal png size %d,%d\n",width,height);
        fprintf(fp,"set output '%s'\n",image_filename);
        fprintf(fp,"plot '%s' using 1:2 with lines\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}

/* Emails for each hour of the week */
void print_volume_weekly(int * week_histogram)
{
    int hr;
    static char* dayname[] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

    printf("Time, Emails\n");
    for (hr=0; hr<24*7; hr++) {
        printf("%s-%02d:00, %d\n",dayname[hr/24],hr%24,week_histogram[hr]);
    }
}

int plot_volume_weekly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * week_histogram,
    int sentiment)
{
    char * plot_filename = "temp_plotfile.plot";
    char * data_filename = "temp_datafile.dat";
    char command[256];
    FILE * fp, * fp_data;
    int hr,retval=0;

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }
    if (file_exists(data_filename)!=0) {
        sprintf((char*)command,"rm %s",data_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fp_data = fopen(data_filename,"w");
        if (fp_data!=NULL) {
            for (hr=0; hr<24*7; hr++) {
                fprintf(fp_data,"%d.%02d, %d\n",hr/24,(hr%24)*100/24,week_histogram[hr]);
            }
            fclose(fp_data);
        }

        fprintf(fp,"reset\n");
        fprintf(fp,"set title %c%s%c\n",(char)34,title,(char)34);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set xlabel %cDay Number%c\n",(char)34,(char)34);
        if (sentiment==0) {
            fprintf(fp,"set ylabel %cTotal Number of Emails%c\n",(char)34,(char)34);
        }
        else {
            fprintf(fp,"set ylabel %cValence%c\n",(char)34,(char)34);
        }
        fprintf(fp,"set grid\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set terminal png size %d,%d\n",width,height);
        fprintf(fp,"set output '%s'\n",image_filename);
        fprintf(fp,"plot '%s' with lines\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}

/* monthly number of emails */
void print_volume_monthly(int * month_histogram)
{
    int i=0,j=0,start_year=0,start_month=0,yr,mnth;
    while ((i<(MAX_YEAR-START_YEAR)*12-1) && (month_histogram[i]<5)) {
        i++;
    }
    start_year = i/12;
    start_month = i%12;
    j=(MAX_YEAR-START_YEAR)*12-1;
    while ((i>0) && (month_histogram[j]<5)) {
        j--;
    }

    printf("Date, Email Volume\n");
    yr=start_year;
    mnth=start_month;
    while (i<=j) {
        printf("01/%02d/%d, %d\n", mnth+1,yr+START_YEAR,month_histogram[i]);
        i++;
        mnth++;
        if (mnth>=12) {
            mnth=0;
            yr++;
        }
    }
}

int plot_volume_monthly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * month_histogram,
    int sentiment)
{
    char * plot_filename = "temp_plotfile.plot";
    char * data_filename = "temp_datafile.dat";
    char command[256];
    FILE * fp, * fp_data;
    int i=0,j=0,start_year=0,start_month=0,yr,mnth,retval=0;

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }
    if (file_exists(data_filename)!=0) {
        sprintf((char*)command,"rm %s",data_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fp_data = fopen(data_filename,"w");
        if (fp_data!=NULL) {
            while ((i<(MAX_YEAR-START_YEAR)*12-1) && (month_histogram[i]<5)) {
                i++;
            }
            start_year = i/12;
            start_month = i%12;
            j=(MAX_YEAR-START_YEAR)*12-1;
            while ((i>0) && (month_histogram[j]<5)) {
                j--;
            }

            yr=start_year;
            mnth=start_month;
            while (i<=j) {
                fprintf(fp_data,"%02d/01/%d, %d\n", mnth+1,yr+START_YEAR,month_histogram[i]);
                i++;
                mnth++;
                if (mnth>=12) {
                    mnth=0;
                    yr++;
                }
            }
            fclose(fp_data);
        }

        fprintf(fp,"reset\n");
        fprintf(fp,"set title %c%s%c\n",(char)34,title,(char)34);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set xlabel %cDate%c\n",(char)34,(char)34);
        fprintf(fp,"set mxtics\n");
        fprintf(fp,"set xtics border out scale 1,1 mirror rotate by -90  offset character 0, 0, 0 autofreq\n");
        if (sentiment==0) {
            fprintf(fp,"set ylabel %cTotal Number of Emails%c\n",(char)34,(char)34);
        }
        else {
            fprintf(fp,"set ylabel %cValence%c\n",(char)34,(char)34);
        }
        fprintf(fp,"set grid\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set xdata time\n");
        fprintf(fp,"set timefmt %c%%m/%%d/%%Y%c\n",(char)34,(char)34);
        fprintf(fp,"set format x %c%%m/%%d/%%Y%c\n",(char)34,(char)34);
        fprintf(fp,"set terminal png size %d,%d\n",width,height);
        fprintf(fp,"set output '%s'\n",image_filename);
        fprintf(fp,"plot '%s' using 1:2 with lines\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}


/* Average number of emails for each month in the year */
void print_volume_annual_average(int * month_histogram)
{
    int i=0,j=0,k,start_month=0,mnth;
    int monthly_sum[12],monthly_hits[12];

    /* clear arrays */
    for (i=0; i<12; i++) {
        monthly_sum[i]=0;
        monthly_hits[i]=0;
    }

    /* get start and end indices */
    while ((i<(MAX_YEAR-START_YEAR)*12-1) && (month_histogram[i]==0)) {
        i++;
    }
    start_month = i%12;
    j=(MAX_YEAR-START_YEAR)*12-1;
    while ((i>0) && (month_histogram[j]==0)) {
        j--;
    }

    /* sum */
    mnth=start_month;
    for (k=i; k<j; k++,mnth++) {
        monthly_sum[mnth]+=month_histogram[k];
        monthly_hits[mnth]++;
        if (mnth>=12) {
            mnth=0;
        }
    }

    /* average */
    for (mnth=0; mnth<12; mnth++) {
        if (monthly_hits[mnth]>0) monthly_sum[mnth]/=monthly_hits[mnth];
    }

    printf("Month, Average Email Volume\n");
    for (mnth=0; mnth<12; mnth++) {
        printf("%d, %d\n", mnth+1, monthly_sum[mnth]);
    }
}

int plot_volume_annual_average(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * month_histogram,
    int sentiment)
{
    int i=0,j=0,k;
    int monthly_sum[12],monthly_hits[12];

    char * plot_filename = "temp_plotfile.plot";
    char * data_filename = "temp_datafile.dat";
    char command[256];
    FILE * fp, * fp_data;
    int start_month=0,mnth,retval=0;
    static char * monthstr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }
    if (file_exists(data_filename)!=0) {
        sprintf((char*)command,"rm %s",data_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fp_data = fopen(data_filename,"w");
        if (fp_data!=NULL) {
            /* clear arrays */
            for (i=0; i<12; i++) {
                monthly_sum[i]=0;
                monthly_hits[i]=0;
            }

            /* get start and end indices */
            while ((i<(MAX_YEAR-START_YEAR)*12-1) && (month_histogram[i]==0)) {
                i++;
            }
            start_month = i%12;
            j=(MAX_YEAR-START_YEAR)*12-1;
            while ((i>0) && (month_histogram[j]==0)) {
                j--;
            }

            /* sum */
            mnth=start_month;
            for (k=i; k<j; k++,mnth++) {
                monthly_sum[mnth]+=month_histogram[k];
                monthly_hits[mnth]++;
                if (mnth>=12) {
                    mnth=0;
                }
            }

            /* average */
            for (mnth=0; mnth<12; mnth++) {
                if (monthly_hits[mnth]>0) monthly_sum[mnth]/=monthly_hits[mnth];
            }

            for (mnth=0; mnth<12; mnth++) {
                fprintf(fp_data,"%s, %d\n", monthstr[mnth], monthly_sum[mnth]);
            }

            fclose(fp_data);
        }

        fprintf(fp,"reset\n");
        fprintf(fp,"set title %c%s%c\n",(char)34,title,(char)34);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set mxtics\n");
        fprintf(fp,"set xtics border out scale 1,1 mirror rotate by 0  offset character 0, 0, 0 autofreq\n");
        fprintf(fp,"set xlabel %cMonth%c\n",(char)34,(char)34);
        if (sentiment==0) {
            fprintf(fp,"set ylabel %cAverage Number of Emails%c\n",(char)34,(char)34);
        }
        else {
            fprintf(fp,"set ylabel %cAverage Valence%c\n",(char)34,(char)34);
        }
        fprintf(fp,"set grid\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set xdata time\n");
        fprintf(fp,"set timefmt %c%%b%c\n",(char)34,(char)34);
        fprintf(fp,"set format x %c%%b%c\n",(char)34,(char)34);
        fprintf(fp,"set terminal png size %d,%d\n",width,height);
        fprintf(fp,"set output '%s'\n",image_filename);
        fprintf(fp,"plot '%s' using 1:2 with lines\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}

/* Number of emails per year */
void print_volume_yearly(int * year_histogram)
{
    int i=0,j=0,start_year=0,yr;
    while ((i<MAX_YEAR-START_YEAR-1) && (year_histogram[i]<5)) {
        i++;
    }
    start_year = i;
    j=(MAX_YEAR-START_YEAR)-1;
    while ((i>0) && (year_histogram[j]<5)) {
        j--;
    }

    printf("Year, Email Volume\n");
    yr=start_year;
    while (i<=j) {
        printf("%d, %d\n", yr+START_YEAR,year_histogram[i]);
        i++;
        yr++;
    }
}

int plot_volume_yearly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * year_histogram,
    int sentiment)
{
    int i=0,j=0,start_year=0,yr;
    char * plot_filename = "temp_plotfile.plot";
    char * data_filename = "temp_datafile.dat";
    char command[256];
    FILE * fp, * fp_data;
    int retval=0;

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }
    if (file_exists(data_filename)!=0) {
        sprintf((char*)command,"rm %s",data_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fp_data = fopen(data_filename,"w");
        if (fp_data!=NULL) {
            while ((i<MAX_YEAR-START_YEAR-1) && (year_histogram[i]<5)) {
                i++;
            }
            start_year = i;
            j=(MAX_YEAR-START_YEAR)-1;
            while ((i>0) && (year_histogram[j]<5)) {
                j--;
            }

            yr=start_year;
            while (i<=j) {
                fprintf(fp_data,"%d, %d\n", yr+START_YEAR,year_histogram[i]);
                i++;
                yr++;
            }
            fclose(fp_data);
        }

        fprintf(fp,"reset\n");
        fprintf(fp,"set title %c%s%c\n",(char)34,title,(char)34);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set mxtics\n");
        fprintf(fp,"set xtics border out scale 1,1 mirror rotate by 0  offset character 0, 0, 0 autofreq\n");
        fprintf(fp,"set xlabel %cYear%c\n",(char)34,(char)34);
        if (sentiment==0) {
            fprintf(fp,"set ylabel %cNumber of Emails%c\n",(char)34,(char)34);
        }
        else {
            fprintf(fp,"set ylabel %cValence%c\n",(char)34,(char)34);
        }
        fprintf(fp,"set grid\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set xdata time\n");
        fprintf(fp,"set timefmt %c%%Y%c\n",(char)34,(char)34);
        fprintf(fp,"set format x %c%%Y%c\n",(char)34,(char)34);
        fprintf(fp,"set terminal png size %d,%d\n",width,height);
        fprintf(fp,"set output '%s'\n",image_filename);
        fprintf(fp,"plot '%s' using 1:2 with lines\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}

contact * print_top(contact * contact_list, int nonames, int min_interactions)
{
    contact * next_contact;

    /* sort by interactions */
    contact_list = sort_contacts(contact_list,SORT_INTERACTIONS);

    printf("Name, Email, Sent, Received, Volume, Influence, Valence\n");

    next_contact = contact_list;
    while (next_contact!=NULL) {
        if ((min_interactions>-1) && (interactions(next_contact)<min_interactions)) break;
        printf("%s, %s, %u, %u, %u, %u, %.3f\n",next_contact->name,next_contact->email,next_contact->sent,next_contact->received,next_contact->sent+next_contact->received,next_contact->influence,next_contact->valence);
        next_contact = (contact*)(next_contact->next);
    }
    return contact_list;
}

contact * print_influence(contact * contact_list, int nonames, int min_interactions)
{
    contact * next_contact;

    /* sort by interactions */
    contact_list = sort_contacts(contact_list,SORT_INFLUENCE);

    printf("Name, Email, Sent, Received, Volume, Influence, Valence\n");

    next_contact = contact_list;
    while (next_contact!=NULL) {
        if ((min_interactions>-1) && (interactions(next_contact)<min_interactions)) break;
        printf("%s, %s, %u, %u, %u, %u, %.3f\n",next_contact->name,next_contact->email,next_contact->sent,next_contact->received,next_contact->sent+next_contact->received,next_contact->influence,next_contact->valence);
        next_contact = (contact*)(next_contact->next);
    }
    return contact_list;
}

void print_valence(contact * contact_list, int nonames, int min_interactions)
{
    contact * next_contact;

    /* sort by interactions */
    contact_list = sort_contacts(contact_list,SORT_VALENCE);

    printf("Name, Email, Sent, Received, Volume, Influence, Valence\n");

    next_contact = contact_list;
    while (next_contact!=NULL) {
        if ((min_interactions>-1) && (interactions(next_contact)<min_interactions)) break;
        printf("%s, %s, %u, %u, %u, %u, %.3f\n",next_contact->name,next_contact->email,next_contact->sent,next_contact->received,next_contact->sent+next_contact->received,next_contact->influence,next_contact->valence);
        next_contact = (contact*)(next_contact->next);
    }
}

/* Save contacts in the graphviz dot format */
void print_dot(
    contact * contact_list,
    int nonames,
    int min_interactions,
    int linewidth,
    int sentiment)
{
    contact * next_contact, * from, * to;
    contact_link * next_link;
    unsigned int size,penwidth=1,max_hits=1,max_inf=0,inf;
    float maxv=-999999,minv=9999999;

    update_IDs(contact_list);

    printf("digraph email {\n");

    clear_selected_flags(contact_list);

    /* Contact links */
    size = (unsigned int)contacts_size(contact_list);
    max_hits = max_contact_link_hits(contact_list);
    if (max_hits<1) max_hits=1;
    next_contact = contact_list;
    while (next_contact!=NULL) {
        next_link = (contact_link*)next_contact->links;
        while (next_link!=NULL) {
            if ((contact*)(next_link->from)==next_contact) {
                from = (contact*)(next_link->from);
                to = (contact*)(next_link->to);
                if ((from!=NULL) && (to!=NULL)) {
                    if ((from==next_contact) && (to->ID<size)) {
                        if ((interactions(from) >= min_interactions) && (interactions(to) >= min_interactions)) {
                            penwidth = (unsigned int)(1+(next_link->hits * linewidth / max_hits));
                            printf("C%u -> C%u [penwidth=%u];\n",from->ID,to->ID,penwidth);
                            from->selected=(char)1;
                            to->selected=(char)1;
                        }
                    }
                }
            }
            next_link = (contact_link*)next_link->next;
        }
        next_contact = (contact*)(next_contact->next);
    }


    next_contact = contact_list;
    while (next_contact!=NULL) {
        /* note the min and max valence values */
        if (next_contact->selected==(char)1) {
            if (next_contact->valence>maxv) maxv=next_contact->valence;
            if (next_contact->valence<minv) minv=next_contact->valence;
        }
        next_contact = (contact*)(next_contact->next);
    }

    /* Contact names */
    if (sentiment==0) {
        max_inf = 1+max_influence(contact_list);
    }
    else {
        if (maxv<=minv) {
            maxv = minv+1;
        }
    }
    next_contact = (contact*)contact_list;
    while (next_contact!=NULL) {
        if (next_contact->selected==(char)1) {

            if (sentiment==0) {
                inf = (unsigned int)(49+(50 - (next_contact->influence*50/max_inf)));
            }
            else {
                if (sentiment>0) {
                    inf = (unsigned int)(49+(50 - ((next_contact->valence-minv)*50/(maxv-minv))));
                }
                else {
                    inf = (unsigned int)(49+(((next_contact->valence-minv)*50/(maxv-minv))));
                }
            }
            if (inf>99) inf=99;
            if (inf<49) inf=49;

            if (next_contact->gender==GENDER_FEMALE) {
                if (nonames==0) {
                    printf("C%u [label=%c%s%c, fillcolor=grey%u];\n",next_contact->ID,(char)34,next_contact->name,(char)34,inf);
                }
                else {
                    printf("C%u [shape=box, fillcolor=grey%u];\n",next_contact->ID,inf);
                }
            }
            else {
                if (nonames==0) {
                    printf("C%u [label=%c%s%c, shape=box, fillcolor=grey%u];\n",next_contact->ID,(char)34,next_contact->name,(char)34,inf);
                }
                else {
                    printf("C%u [shape=box, fillcolor=grey%u];\n",next_contact->ID,inf);
                }
            }
        }
        next_contact = (contact*)(next_contact->next);
    }

    printf("}\n");
}

int plot_email_dates(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    char * data_filename)
{
    char * plot_filename = "temp_plotfile.plot";
    char command[256];
    FILE * fp;
    int retval=0;

    if (file_exists(plot_filename)!=0) {
        sprintf((char*)command,"rm %s",plot_filename);
        retval=system(command);
    }

    fp = fopen(plot_filename,"w");
    if (fp!=NULL) {

        fprintf(fp,"reset\n");
        fprintf(fp,"set title \"%s\"\n",title);
        if (subtitle != NULL) fprintf(fp,"set label '%s' at screen 0.02, screen 0.02\n",subtitle);
        fprintf(fp,"set lmargin 9\n");
        fprintf(fp,"set rmargin 2\n");
        fprintf(fp,"set xlabel \"Date\"\n");
        fprintf(fp,"set ylabel \"Time\"\n");
        fprintf(fp,"set yrange [0:24]\n");
        fprintf(fp,"set key off\n");
        fprintf(fp,"set xdata time\n");
        fprintf(fp,"set format x \"%%m-%%Y\"\n");
        fprintf(fp,"set timefmt \"%%m-%%d-%%Y\"\n");
        fprintf(fp,"set xtics rotate by -90\n");
        fprintf(fp,"set terminal png size %d,%d font courier 10\n",width,height);
        fprintf(fp,"set output \"%s\"\n",image_filename);
        fprintf(fp,"plot \"%s\" using 1:2 with points\n",data_filename);
        fclose(fp);

        sprintf((char*)command,"gnuplot %s",plot_filename);
        retval=system(command);
    }
    return retval;
}
