/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

void corpus_free(filename_list * email_list)
{
    filename_list * tmp;
    filename_list * next_email = email_list;
    while (next_email!=NULL) {
        tmp = next_email;
        next_email = (filename_list*)(next_email->next);
        free((void*)tmp);
    }
}

void corpus_update(char * mbox_filename, int start_line_number, int end_line_number, filename_list * email_list)
{
    filename_list * tmp = NULL;
    filename_list * c = (filename_list*)malloc(sizeof(filename_list));
    filename_list * next_email = email_list;

    if (c!=NULL) {
        sprintf(c->filename,"%s",mbox_filename);
        c->start_line_number = start_line_number;
        c->end_line_number = end_line_number;
        c->next=NULL;

        while (next_email != NULL) {
            tmp = next_email;
            next_email = (filename_list*)(next_email->next);
        }
        if (tmp!=NULL) tmp->next=(struct filename_list*)c;
    }
}

void corpus_extract(filename_list * email_list)
{
    FILE * fp=NULL;
    filename_list * next_email = (filename_list*)(email_list->next);
    filename_list * prev=NULL;
    int line_number=0;
    char * retval=NULL;
    char linestr[256];

    while (next_email!=NULL) {

        printf("------%s %d %d -----\n",next_email->filename,next_email->start_line_number,next_email->end_line_number);

        if (prev!=NULL) {
            if (strcmp(prev->filename,next_email->filename)!=0) {
                fp = fopen(next_email->filename,"r");
                line_number=0;
            }
        }
        else {
            fp = fopen(next_email->filename,"r");
            line_number=0;
        }
        if (fp!=NULL) {
            while ((!feof(fp)) && (line_number<next_email->end_line_number)) {
                retval = fgets(linestr,255,fp);
                if (retval != NULL) {
                    if (line_number>=next_email->start_line_number) {
                        printf("%s",linestr);
                    }
                    line_number++;
                }
                else {
                    break;
                }
            }
        }

        prev = next_email;
        next_email = (filename_list*)(next_email->next);
        if (next_email!=NULL) {
            if (strcmp(prev->filename,next_email->filename)!=0) {
                fclose(fp);
            }
        }
        else {
            fclose(fp);
        }
    }
}
