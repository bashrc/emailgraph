/*
  Email graphing
  Copyright (C) 2011 Bob Mottram
  bob@robotics.uk.to

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EMAILGRAPH_H
#define EMAILGRAPH_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define VERSION 1.00
#undef DEBUG
#undef STYLOMETRICS

#define CONTACT_BUFFER_SIZE    20
#undef CONTACT_EVENTS

#define STRING_BUFFER_MAX      256
#define LIST_BUFFER_MAX        256

#define NUM_OF(x)              (sizeof (x) / sizeof *(x))

#define CONTACT_NAME_SIZE      50
#define CONTACT_EMAIL_SIZE     50

#define IMAGE_WIDTH            1024
#define IMAGE_HEIGHT           480

#define START_YEAR             1970
#define MAX_YEAR               2030

#define SORT_NAMES             0
#define SORT_INTERACTIONS      1
#define SORT_INFLUENCE         2
#define SORT_VALENCE           3

#define ENTRY_FROM             0
#define ENTRY_TO               1
#define ENTRY_CC               2

#define FROM                   "From: "
#define TO                     "To: "
#define CC                     "Cc: "
#define DATE                   "Date: "
#define SUBJECT                "Subject: "


/*directory */
typedef struct {
    char filename[256];
    int start_line_number,end_line_number;
    struct filename_list * next;
} filename_list;

typedef struct {
    char fromtext[256];
    char totext[256];
    struct merge_rule * next;
} merge_rule;

#ifdef STYLOMETRICS
typedef struct {
    unsigned int sentence_length;
    unsigned int sentences;
    unsigned int word_length;
    unsigned int words;
    unsigned int punctuation[8];
    unsigned int emails;
} stylometrics;
#endif

typedef struct {
    struct contact * from;
    struct contact * to;
    unsigned int hits;
    struct contact_link * next;
    struct contact_link * previous;
} contact_link;

typedef struct {
    struct tm date;
    struct contact * from;
    struct contact * to;
    struct contact_event * next;
    struct contact_event * previous;
} contact_event;

typedef struct {
    unsigned int ID;
    unsigned int hash;
    char selected;
    char gender;
    char name[CONTACT_NAME_SIZE];
    char email[CONTACT_EMAIL_SIZE];
    unsigned int sent,received,influence;
    float valence;
#ifdef STYLOMETRICS
    stylometrics style;
#endif
    struct contact_event * events;
    struct contact_link *links;
    struct contact * next, * previous;
} contact;

/* tests.c */
void run_tests();

/* dates.c */
int valid_date(struct tm date);
int day_of_week(int month_day, int month, int year);
unsigned int datedays(struct tm date);
struct tm parse_date(char * date_str);

/* counting.c */
int links_size(contact * c);
int contacts_size(contact * contact_list);
unsigned int max_contact_link_hits(contact * contact_list);
unsigned int max_influence(contact * contact_list);
int max_valence(contact * contact_list);
int min_valence(contact * contact_list);

/* events.c */
void free_events(contact * c);
void remove_contact_event(contact_event * event);
void add_contact_event(contact * base, contact* from, contact * to, struct tm contact_date);
void add_contact_events(
    contact ** fromcontacts,
    contact ** tocontacts,
    struct tm contact_date);
int contains_event(contact * c, contact * from, contact * to, struct tm contact_date);
void merge_events(contact * new_contact, contact * old_contact);

/* display.c */
int file_exists(char * filename);
void print_help();
contact * print_influence(contact * contact_list, int nonames, int min_interactions);
void print_valence(contact * contact_list, int nonames, int min_interactions);
void print_contacts(contact * contact_list);
void update_IDs(contact * contact_list);
int interactions(contact * c);
void print_volume_daily(int * day_histogram);
int plot_email_dates(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    char * data_filename);
int plot_volume_daily(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * day_histogram,
    int sentiment);
void print_volume_weekly(int * week_histogram);
void print_volume_monthly(int * month_histogram);
int plot_volume_weekly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * week_histogram,
    int sentiment);
int plot_volume_monthly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * month_histogram,
    int sentiment);
void print_volume_annual_average(int * month_histogram);
int plot_volume_annual_average(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * month_histogram,
    int sentiment);
void print_volume_yearly(int * year_histogram);
int plot_volume_yearly(
    char * image_filename,
    char * title, char * subtitle,
    int width, int height,
    int * year_histogram,
    int sentiment);
contact * print_top(contact * contact_list, int nonames, int min_interactions);
void print_dot(contact * contact_list, int nonames, int min_interactions, int linewidth, int sentiment);

/* hashes */
unsigned int hash_value(contact * c, int sort_type);
contact * update_hashes(contact * contact_list, int sort_type);

/* contacts.c */
void copy_contact(contact * c, contact * other);
int contacts_contains(contact * search, contact * contact_list);
void init_contact(contact * c);
void clear_selected_flags(contact * contact_list);
int compare_contacts(contact * c1, contact * c2);
contact * min_contact(contact * c);
void insert_contact(contact * previous, contact * current, contact * next);
void swap_contacts(contact * old_contact, contact * new_contact);
contact * sort_contacts(contact * contact_list, int sort_type);
void remove_contact(contact * c);
contact * remove_non_names(contact * contact_list);
contact * merge_contacts(contact * c, contact * other, contact * contact_list);
contact * find_good_name(contact *contact_list, char * email, int no_at);
contact * merge_duplicates(contact *contact_list);
contact * get_contact(contact * contact_list, int index);
contact * find_contact(contact * c, contact * contact_list);
contact * find_contact_email(contact * c, contact * contact_list);
void free_contacts(contact * contact_list);
contact * add_contact(contact * c, contact * contact_list, int entry_type);
void add_contacts(contact * contacts, contact * contact_list, int entry_type, contact ** result, int valence);

/* influence */
unsigned int spread_contact_influence(contact * c);
unsigned int contact_influence(contact * c, contact * contact_list);
void update_influence(contact * contact_list);

/* links.c */
void update_links(contact * contact_list);
void free_links(contact * c);
void remove_links(contact * c);
void remove_contact_link(contact_link * link);
void add_contact_link(contact * base, contact* from, contact * to);
int add_contact_links(
    contact ** fromcontacts,
    contact ** tocontacts,
    contact ** cccontacts,
    int gender);
void replace_link_contact(contact * base, contact * old_contact, contact * new_contact);
void consolidate_contact_links(contact * c);
void merge_links(contact * new_contact, contact * old_contact);
void consolidate_links(contact * contact_list);

/* stylometrics.c */
void clear_style();
void set_style(contact * c);
void update_style(contact * c, contact * other);
int stylometrics_process_sentence(char * sentence, int * unused);

/* semantics.c */
int semantics_process_sentence(char * sentence, int * unused);

/* read_email.c */
struct tm read_email_date(char * linestr);
void read_email_address(char * linestr, contact * email);
int read_email_contact(char * linestr, int entry_type, contact * contact_list, int gender);
contact * read_from_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_from);
contact * read_to_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_to);
contact * read_cc_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_cc);
void process_emails(
    char * filename,
    contact * contact_list,
    struct tm start_date, struct tm end_date,
    int start_hour, int end_hour,
    int * month_histogram,
    int * year_histogram,
    int * day_histogram,
    int * week_histogram,
    char ** keywords,
    int gender,
    filename_list * extract_corpus,
    int sentence_dump,
    int sentiment,
    FILE * fp_dates);

/* corpus.c */
void corpus_free(filename_list * email_list);
void corpus_extract(filename_list * email_list);
void corpus_update(char * mbox_filename, int start_line_number, int end_line_number, filename_list * email_list);

/* merge.c */
void merge_read_file(char * filename, merge_rule * rules);
void merge_free(merge_rule * rules);
void merge_contacts_with_rules(contact * contact_list, merge_rule * rules);

/* words.c */
int ContainsNonChars(char * ch);
int ContainsWord(char * ch, char * search);
int CountChars(char * ch, char search);
int ContainsChar(char * ch, char search);
void Trim(char * ch, char c);
int IsNumber(char c);
char * ToLower(char * ch);
char * ToUpper(char * ch);
int IndexOf(char ** words, char * word);
void Split(char * ch, char separator, char ** result);
void TextOnly(char * ch, char * result);
void RemoveCommonWords(
    char * text,
    char ** common_words,
    char * string_buffer,
    char ** list_buffer,
    char * result);
int contains_keywords(char * linestr, char ** keywords);
int detect_sentence(
    char * text,
    int state,
    char * buffer, int * buffer_ctr,
    int * value,
    int (*fn_process_sentence)(char*,int*));
int detect_words(
    char * sentence,
    char * buffer,
    int * value,
    void (*fn_process_word)(char*,int*));

/* exclude.c */
void exclude_read_file(char * filename, merge_rule * rules);
void exclude_free(merge_rule * rules);
void exclude_contacts_with_rules(contact * contact_list, merge_rule * rules);

/* sentiment.c */
int read_sentiment_file(char * filename, int word_array);
int sentiment_process_sentence(char * sentence, int * valence);
void update_valence(contact * contact_list);

enum {
    WALK_OK = 0,
    WALK_BADPATTERN,
    WALK_NAMETOOLONG,
    WALK_BADIO
};

#define WS_NONE		0
#define WS_RECURSIVE	(1 << 0)
#define WS_DEFAULT	WS_RECURSIVE
#define WS_FOLLOWLINK	(1 << 1)	/* follow symlinks */
#define WS_DOTFILES	(1 << 2)	/* per unix convention, .file is hidden */
#define WS_MATCHDIRS	(1 << 3)	/* if pattern is used on dir names too */

int walk_dir(char *dname, char *pattern, int spec, filename_list * filenames);

#define GENDER_UNKNOWN     0
#define GENDER_MALE        1
#define GENDER_FEMALE      2
#define GENDER_MALE_FEMALE 3

int get_gender(char * ch);

#endif
