/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

unsigned int hash_value(contact * c, int sort_type)
{
    int i;
    double mult=10000000,result=0;
    char * hashstr,ch;

    switch(sort_type) {
    case SORT_NAMES: {
        hashstr = c->name;
        for (i=0; i<strlen(hashstr); i++) {
            ch = toupper(hashstr[i]);
            if ((ch>='A') && (ch<='Z')) {
                result += (1+ch-'A')*mult;
                mult/=26.0f;
            }
        }
        break;
    }
    case SORT_INTERACTIONS: {
        result = 10000-(c->sent + c->received);
        break;
    }
    case SORT_INFLUENCE: {
        result = 1000000 - c->influence;
        break;
    }
    case SORT_VALENCE: {
        result = 1000000 - (c->valence*100);
        break;
    }
    }
    return (unsigned int)result;
}

contact * update_hashes(contact * contact_list, int sort_type)
{
    int i=0;
    contact * next_contact,*temp_next;

    /* update hashes */
    next_contact = contact_list;
    while (next_contact!=NULL) {
        next_contact->hash = hash_value(next_contact,sort_type);
        if (next_contact->hash==0) {
            temp_next = (contact*)(next_contact->next);
            if (next_contact==contact_list) contact_list=temp_next;
            remove_contact(next_contact);
            next_contact = temp_next;
        }
        else {
            next_contact = (contact*)(next_contact->next);
        }
        i++;
    }
    return contact_list;
}
