/*
  Email graphing
  Copyright (C) 2011 Bob Mottram
  bob@robotics.uk.to

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* Reading dates from file -----------------------------------------------------------------*/

/* Reads a date and time from a line. <day number> <3 character month> <4 character year> H:M
   This is primitive, and no doubt better is possible */
struct tm read_email_date_format1(char * linestr)
{
    struct tm t;
    int i=0,n=0,n2=0,j=0,mnth=0,year_index=0,d=0;
    char buffer[256],buffer2[6];
    const char * month[] = {
        "Jan", "Feb", "Apr", "Mar", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    t.tm_year=0;
    t.tm_mon=0;
    t.tm_mday=0;
    t.tm_hour=0;
    t.tm_min=0;
    memset((void*)&t,'\0',sizeof(struct tm));
    memset((void*)buffer,'\0',256);
    memset((void*)buffer2,'\0',6);

    while ((i<strlen(linestr)-1) && (linestr[i]!=' ')) i++;
    if (i<strlen(linestr)-1) {
        i++;
        while ((linestr[i]!='\0') && (n<255)) {
            buffer[n++]=linestr[i];
            i++;
        }
        buffer[n]='\0';

        /* get the month */
        for (i=0; i<n-3; i++) {
            for (mnth=0; mnth<12; mnth++) {
                for (j=0; j<3; j++) {
                    if (buffer[i+j]!=month[mnth][j]) break;
                }
                if (j==3) {
                    ;
                    t.tm_mon = mnth+1;
                    i=n;
                    break;
                }
            }
        }

        /* get the year */
        for (i=0; i<n-4; i++) {
            n2=0;
            for (j=0; j<4; j++) {
                if ((buffer[i+j]<'0') || (buffer[i+j]>'9')) break;
                buffer2[n2++]=buffer[i+j];
            }
            if (j==4) {
                buffer2[n2]='\0';
                year_index=i;
                t.tm_year = atoi(buffer2);
                if (t.tm_year<100) {
                    if (t.tm_year>=70) {
                        t.tm_year+=1900;
                    }
                    else {
                        t.tm_year+=2000;
                    }
                }
                break;
            }
        }

        /* get the day */
        for (i=0; i<n-3; i++) {
            if (((i<year_index) || (i>year_index+4)) &&
                    (IsNumber(buffer[i]))) {
                n2=0;
                for (j=0; j<2; j++) {
                    if (IsNumber(buffer[i+j])) {
                        buffer2[n2++]=buffer[i+j];
                    }
                }
                buffer2[n2]='\0';
                d = atoi(buffer2);
                if ((d>0) && (d<32)) {
                    t.tm_mday = d;
                    break;
                }
            }
        }

        /* get the time */
        for (i=0; i<n-6; i++) {
            if ((buffer[i]==' ') &&
                    (IsNumber(buffer[i+1])) && (IsNumber(buffer[i+2])) &&
                    (buffer[i+3]==':') &&
                    (IsNumber(buffer[i+4])) && (IsNumber(buffer[i+5]))) {
                buffer2[0] = buffer[i+1];
                buffer2[1] = buffer[i+2];
                buffer2[2] = '\0';
                t.tm_hour = atoi(buffer2);
                buffer2[0] = buffer[i+4];
                buffer2[1] = buffer[i+5];
                buffer2[2] = '\0';
                t.tm_min = atoi(buffer2);
                break;
            }
        }

        t.tm_sec = 1;
        t.tm_isdst = 0;

#ifdef DEBUG
        printf("Date: %s (%d/%d/%d %d:%d)\n", buffer,t.tm_mday,t.tm_mon,t.tm_year,t.tm_hour,t.tm_min);
#endif
    }
    return t;
}

/* Reads a date and time from a line. <month number>/<day number>/<2 or 4 character year> H:M
   This is primitive, and no doubt better is possible */
struct tm read_email_date_format2(char * linestr)
{
    int i=0,j=0,n=0;
    struct tm result;
    char buffer[6];

    memset((void*)&result,'\0',sizeof(struct tm));
    result.tm_year=0;
    result.tm_mon=0;
    result.tm_mday=0;
    result.tm_hour=0;
    result.tm_min=0;
    result.tm_sec=1;

    while ((i<strlen(linestr)-1) && (linestr[i]!='/')) {
        i++;
    }
    if (i<strlen(linestr)-1) {
        j=i-1;
        while ((j>0) && (linestr[j]!=' ')) j--;
        if (linestr[j]==' ') {

            j++;
            n=0;
            while ((linestr[j]!='/') && (i<strlen(linestr)-1) && (n<5)) {
                buffer[n++]=linestr[j];
                j++;
            }
            buffer[n]='\0';
            result.tm_mon=atoi(buffer);

            j++;
            n=0;
            while ((linestr[j]!='/') && (i<strlen(linestr)-1) && (n<5)) {
                buffer[n++]=linestr[j];
                j++;
            }
            buffer[n]='\0';
            result.tm_mday=atoi(buffer);

            j++;
            n=0;
            while ((linestr[j]!=' ') && (i<strlen(linestr)-1) && (n<5)) {
                buffer[n++]=linestr[j];
                j++;
            }
            buffer[n]='\0';
            result.tm_year=atoi(buffer);
            if (result.tm_year<100) {
                if (result.tm_year>=70) {
                    result.tm_year+=1900;
                }
                else {
                    result.tm_year+=2000;
                }
            }

            /* get the time */
            for (i=0; i<strlen(linestr)-6; i++) {
                if ((linestr[i]==' ') &&
                        (IsNumber(linestr[i+1])) && (IsNumber(linestr[i+2])) &&
                        (linestr[i+3]==':') &&
                        (IsNumber(linestr[i+4])) && (IsNumber(linestr[i+5]))) {
                    buffer[0] = linestr[i+1];
                    buffer[1] = linestr[i+2];
                    buffer[2] = '\0';
                    result.tm_hour = atoi(buffer);
                    buffer[0] = linestr[i+4];
                    buffer[1] = linestr[i+5];
                    buffer[2] = '\0';
                    result.tm_min = atoi(buffer);
                    break;
                }
            }

        }
    }

    return result;
}

struct tm read_email_date(char * linestr)
{
    if (CountChars(linestr,'/')!=2) {
        return read_email_date_format1(linestr);
    }
    else {
        return read_email_date_format2(linestr);
    }
}

/* Reading email address from file -----------------------------------------------------------------*/

/* reads an email address from the line */
void read_email_address(char * linestr, contact * email)
{
    int i=0,j=0,n=0;

    email->email[0] = '\0';

    while ((i<strlen(linestr)-1) && (linestr[i]!='@')) i++;
    if (i<strlen(linestr)-1) {
        j=i;
        while ((j>0) && (linestr[j]!=':') && (linestr[j]!=' ') && (linestr[j]!='<') && (linestr[j]!='[')) j--;
        j++;
        while ((j<255) && (n<CONTACT_EMAIL_SIZE-1) && (linestr[j]!='\0') && (linestr[j]!=' ') && (linestr[j]!='>') && (linestr[j]!=']')) {
            if ((linestr[j]!=39) && (linestr[j]!='"') && (linestr[j]!=',') && (linestr[j]!=';') && (linestr[j]!=13) && (linestr[j]!=10) && (linestr[j]!=9)) {
                email->email[n++]=tolower(linestr[j]);
            }
            j++;
        }
        if (n>=CONTACT_EMAIL_SIZE) printf("read_email_address: array index too large\n");
        email->email[n]='\0';
#ifdef DEBUG
        printf("Email: %s\n", email->email);
#endif
    }
    Trim(email->email,' ');
}

/* Reading contact name ------------------------------------------------------------------------*/

/* reads the name for a From: or To: line */
void read_email_name(char * linestr, contact * email, int gender)
{
    int i=0,j=0,n=0;

#ifdef DEBUG
    printf("%s\n",linestr);
#endif

    email->name[0] = '\0';

    while ((i<strlen(linestr)-1) && (linestr[i]!=':')) i++;
    j=i;
    while ((j<strlen(linestr)-1) && (linestr[j]!='"')) j++;
    if (j<strlen(linestr)-1) {
        j++;
        /* name surrounded by quotes */
        n=0;
        while ((j<strlen(linestr)-1) && (linestr[j]!='"') && (n<CONTACT_NAME_SIZE-1)) {
            if ((linestr[j]!=9) && (linestr[j]!=39) && (linestr[j]!='\\')) {
                email->name[n++]=linestr[j];
            }
            j++;
        }
        if (n>=CONTACT_NAME_SIZE) printf("read_email_name:1: array index too large\n");
        email->name[n]='\0';
#ifdef DEBUG
        printf("Name: %s\n", email->name);
#endif
    }
    else {
        /* no quotes around name */
        j=i+1;
        while ((j<strlen(linestr)-2) && (linestr[j]==' ')) j++;
        if (j<strlen(linestr)-2) {
            n=0;
            while ((j<strlen(linestr)-1) && (linestr[j]!='<') &&
                    (linestr[j]!='[') && (linestr[j]!='\0') &&
                    (linestr[j]!=13) && (linestr[j]!=10) && (n<CONTACT_NAME_SIZE-1)) {
                if ((linestr[j]!=9) && (linestr[j]!=39) && (linestr[j]!='\\')) {
                    email->name[n++]=linestr[j];
                }
                j++;
            }
            if (j<strlen(linestr)-1) {
                while ((n>0) && (email->name[n]==' ')) n--;
                if ((n<CONTACT_NAME_SIZE-1) && (email->name[n]!=' ') && (email->name[n-1]!=' ')) n++;
                if (n>=CONTACT_NAME_SIZE) printf("read_email_name:2: array index too large\n");
                email->name[n]='\0';
#ifdef DEBUG
                printf("Name: %s\n", email->name);
#endif
            }
            else {
                if (n>=CONTACT_NAME_SIZE) printf("read_email_name:3: array index too large\n");
                email->name[n]='\0';
            }
        }
    }

    Trim(email->name,' ');
    email->gender=GENDER_UNKNOWN;
    if (email->name[0]!='\0') {
        email->gender=get_gender(email->name);
    }
    if ((gender!=GENDER_UNKNOWN) && (gender!=GENDER_MALE_FEMALE)) {
        if (gender != email->gender) email->name[0]='\0';
    }
}

/* If a name is given in reverse this reorders.  eg. "Smith, John" becomes "John Smith" */
void reorder_name(char * name)
{
    int i=0,j=0,n=0,cut=0;
    char new_name[CONTACT_NAME_SIZE];

    if (ContainsChar(name,'|')!=0) {
        for (i=0; i<strlen(name); i++) {
            if (name[i]=='|') break;
        }
        if (i<strlen(name)-2) {
            j=i+1;
            while ((name[j]==' ') && (j<strlen(name)-1)) j++;
            while ((j<strlen(name)) && (n<CONTACT_NAME_SIZE-2)) {
                new_name[n++]=name[j++];
            }
            new_name[n++]=' ';
            for (j=0; j<i; j++) {
                if (n<CONTACT_NAME_SIZE-2) new_name[n++]=name[j];
            }
            new_name[n]='\0';
            if (n>4) {
                /* Phd */
                if ((toupper(new_name[0])=='P') && (toupper(new_name[1])=='H') &&
                        (toupper(new_name[2])=='D') &&
                        ((toupper(new_name[3])==' ') || (toupper(new_name[3])=='.'))) {
                    cut=1;
                }
                /* Bsc */
                if ((toupper(new_name[0])=='B') && (toupper(new_name[1])=='S') &&
                        (toupper(new_name[2])=='C') &&
                        ((toupper(new_name[3])==' ') || (toupper(new_name[3])=='.'))) {
                    cut=1;
                }
                /* Inc */
                if ((toupper(new_name[0])=='I') && (toupper(new_name[1])=='N') &&
                        (toupper(new_name[2])=='C') &&
                        ((toupper(new_name[3])==' ') || (toupper(new_name[3])=='.'))) {
                    cut=1;
                }
                if (cut!=0) {
                    for (i=0; i<strlen(name); i++) {
                        if (name[i]=='|') {
                            name[i]='\0';
                            break;
                        }
                    }
                    return;
                }
            }
            for (i=0; i<n+1; i++) {
                name[i]=new_name[i];
            }
        }
    }
}

/* reads a From: or To: line from an email */
int read_email_contact(char * linestr, int entry_type, contact * contact_list, int gender)
{
    int i=0,j=0,n=0,quote=0,reading=0,found=0,hits=0;
    char * contacts[256];
    char buffer[256];
    contact * email = NULL;
    contact * contact_list_0=contact_list;

    contact_list->next=NULL;

    /* replace commas inside of quotes */
    for (i=0; i<strlen(linestr); i++) {
        if (linestr[i]=='"') quote=1-quote;
        if ((linestr[i]==',') && (quote==1)) linestr[i]='|';
    }

    /* split multiple addresses */
    if (ContainsChar(linestr,';')==0) {
        Split(linestr, ',', contacts);
    }
    else {
        Split(linestr, ';', contacts);
    }

    i=0;
    while (contacts[i]!=NULL) {
        j=n=0;

        if (i>0) {
            if (entry_type==ENTRY_FROM) {
                sprintf((char*)buffer,"%s",FROM);
                n = strlen(FROM);
            }
            if (entry_type==ENTRY_TO) {
                sprintf((char*)buffer,"%s",TO);
                n = strlen(TO);
            }
            if (entry_type==ENTRY_CC) {
                sprintf((char*)buffer,"%s",TO);
                n = strlen(TO);
            }
        }

        reading=0;
        while ((contacts[i][j]!=',') && (contacts[i][j]!=';') && (contacts[i][j]!='\0') && (n<255)) {
            if ((contacts[i][j]!=' ') && (reading==0)) {
                reading=1;
            }
            if (reading!=0) {
                buffer[n++] = contacts[i][j];
            }
            j++;
        }
        buffer[n]='\0';

        if (j>5) {

            found=0;
            if (contact_list_0->name[0]!='\0') {
                email = (contact*)malloc(sizeof(contact));
            }
            else {
                email = contact_list_0;
            }
            if (email!=NULL) {
                email->name[0]='\0';
                read_email_name(buffer, email, gender);
                if ((email->name[0]!='\0') &&
                        (strlen(email->name)>5) &&
                        (ContainsNonChars(email->name)==0) &&
                        (ContainsChar(email->name,'=')==0) &&
                        (ContainsChar(email->name,'%')==0) &&
                        (ContainsChar(email->name,'&')==0) &&
                        (ContainsChar(email->name,'?')==0)) {
                    read_email_address(buffer, email);
                    if ((strlen(email->email)>5) &&
                            (ContainsChar(email->email,'@'))) {
                        reorder_name(email->name);
                        email->events=NULL;
                        email->links=NULL;
                        found=1;
                        hits++;
                        if (email!=contact_list) {
                            email->previous=(struct contact *)contact_list;
                            contact_list->next = (struct contact *)email;
                            contact_list = email;
                        }
                    }
                }

                if (found==0) {
                    email->name[0]='\0';
                    if (email!=contact_list_0) {
                        free(email);
                        email=NULL;
                    }
                }
            }
            else {
                fprintf(stderr,"read_email_contact: unable to allocate memory\n");
            }
        }
        i++;
    }
    contact_list->next=NULL;
    return hits;
}

/* reading lines ----------------------------------------------------------------------------------*/

/* read the From: line */
contact * read_from_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_from)
{
    if (strncmp(buffer,FROM,start_index)==0) {
        if (contact_from!=NULL) {
            free_contacts(contact_from);
            contact_from=NULL;
        }
        contact_from = (contact*)malloc(sizeof(contact));
        if (contact_from!=NULL) {
            init_contact(contact_from);
            if (read_email_contact(linestr,ENTRY_FROM, contact_from, gender)==0) {
                free(contact_from);
                contact_from=NULL;
            }
        }
        else {
            fprintf(stderr,"update_graph/contact_from: unable to allocate memory\n");
        }
    }
    return contact_from;
}

/* read the To: line */
contact * read_to_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_to)
{
    if (strncmp(buffer,TO,start_index)==0) {
        if (contact_to!=NULL) {
            free_contacts(contact_to);
            contact_to=NULL;
        }
        contact_to = (contact*)malloc(sizeof(contact));
        if (contact_to!=NULL) {
            init_contact(contact_to);
            if (read_email_contact(linestr,ENTRY_TO, contact_to, gender)==0) {
                free(contact_to);
                contact_to=NULL;
            }
        }
        else {
            fprintf(stderr,"update_graph/contact_to: unable to allocate memory\n");
        }
    }
    return contact_to;
}

/* read the Cc: line */
contact * read_cc_line(char * linestr, char * buffer, int start_index, int gender, contact * contact_cc)
{
    if (strncmp(buffer,CC,start_index)==0) {
        if (contact_cc!=NULL) {
            free_contacts(contact_cc);
            contact_cc=NULL;
        }
        contact_cc = (contact*)malloc(sizeof(contact));
        if (contact_cc!=NULL) {
            init_contact(contact_cc);
            if (read_email_contact(linestr,ENTRY_CC, contact_cc, gender)==0) {
                free(contact_cc);
                contact_cc=NULL;
            }
        }
        else {
            fprintf(stderr,"update_graph/contact_cc: unable to allocate memory\n");
        }
    }
    return contact_cc;
}

/* Processing emails --------------------------------------------------------------------*/

contact * contact_from=NULL, * contact_to=NULL, * contact_cc=NULL;
contact * fromcontacts[CONTACT_BUFFER_SIZE];
contact * tocontacts[CONTACT_BUFFER_SIZE];
contact * cccontacts[CONTACT_BUFFER_SIZE];

/* process an individual email */
void process_email(
    contact * contact_list,
    struct tm contact_date,
    struct tm start_date, struct tm end_date,
    int start_hour, int end_hour,
    int * month_histogram,
    int * year_histogram,
    int * day_histogram,
    int * week_histogram,
    char ** keywords,
    int gender,
    int keywords_found,
    int valence,
    int sentiment,
    FILE * fp_dates)
{
    int hist_index=0;

    /* between given dates */
    if ((contact_from!=NULL) && ((contact_to!=NULL) || (contact_cc!=NULL)) &&
            (valid_date(contact_date)!=0) &&
            ((keywords[0]==NULL) || ((keywords[0]!=NULL) && (keywords_found!=0))) &&
            ((start_date.tm_year==0) ||
             ((start_date.tm_year>0) && (end_date.tm_year==0) && (datedays(contact_date)>=datedays(start_date))) ||
             ((start_date.tm_year>0) && (end_date.tm_year>0) && (datedays(contact_date)>=datedays(start_date)) && (datedays(contact_date)<=datedays(end_date))) )) {

        /* Between given hours */
        if ((start_hour==-1) ||
                ((start_hour!=-1) && (end_hour==-1) && (contact_date.tm_hour>=start_hour)) ||
                ((start_hour!=-1) && (end_hour!=-1) && (contact_date.tm_hour>=start_hour) && (contact_date.tm_hour<=end_hour))) {

            if (fp_dates!=NULL) {
                fprintf(fp_dates,"%02d-%02d-%04d, %.2f\n",
                        contact_date.tm_mon, contact_date.tm_mday, contact_date.tm_year,
                        contact_date.tm_hour + (contact_date.tm_min/60.0f));
            }

            fromcontacts[0]=NULL;
            if (contact_from!=NULL) {
                add_contacts(contact_from,contact_list,ENTRY_FROM,fromcontacts,valence);
            }

            tocontacts[0]=NULL;
            if (contact_to!=NULL) {
                add_contacts(contact_to,contact_list,ENTRY_TO,tocontacts,0);
            }

            cccontacts[0]=NULL;
            if (contact_cc!=NULL) {
                add_contacts(contact_cc,contact_list,ENTRY_CC,cccontacts,0);
            }

#ifdef CONTACT_EVENTS
            add_contact_events(fromcontacts, tocontacts, contact_date);
#endif
            if (add_contact_links(fromcontacts, tocontacts, cccontacts, gender)!=0) {
                /* update monthly volume histogram */
                hist_index = ((contact_date.tm_year-START_YEAR)*12)+contact_date.tm_mon-1;
                if ((hist_index>=0) && (hist_index < (MAX_YEAR-START_YEAR)*12)) {
                    if (sentiment==0) {
                        month_histogram[hist_index]++;
                    }
                    else {
                        if (sentiment>0) {
                            month_histogram[hist_index]+=valence;
                        }
                        else {
                            if (valence<0) {
                                month_histogram[hist_index]-=valence;
                            }
                        }
                    }
                }

                /* update yearly volume histogram */
                hist_index = (contact_date.tm_year-START_YEAR);
                if ((hist_index>=0) && (hist_index < (MAX_YEAR-START_YEAR))) {
                    if (sentiment==0) {
                        year_histogram[hist_index]++;
                    }
                    else {
                        if (sentiment>0) {
                            year_histogram[hist_index]+=valence;
                        }
                        else {
                            if (valence<0) {
                                year_histogram[hist_index]-=valence;
                            }
                        }
                    }
                }

                /* daily average histogram */
                if ((contact_date.tm_hour>=0) && (contact_date.tm_hour<24)) {
                    if (sentiment==0) {
                        day_histogram[contact_date.tm_hour]++;
                    }
                    else {
                        if (sentiment>0) {
                            day_histogram[contact_date.tm_hour]+=valence;
                        }
                        else {
                            if (valence<0) {
                                day_histogram[contact_date.tm_hour]-=valence;
                            }
                        }
                    }
                }

                /* weekly average histogram */
                hist_index=(day_of_week(contact_date.tm_mday,contact_date.tm_mon,contact_date.tm_year)*24)+contact_date.tm_hour;
                if ((hist_index>=0) && (hist_index<24*7)) {
                    if (sentiment==0) {
                        week_histogram[hist_index]++;
                    }
                    else {
                        if (sentiment>0) {
                            week_histogram[hist_index]+=valence;
                        }
                        else {
                            if (valence<0) {
                                week_histogram[hist_index]-=valence;
                            }
                        }
                    }
                }
            }
        }
    }

    /* free lists */
    if (contact_from!=NULL) {
        free_contacts(contact_from);
        contact_from=NULL;
    }
    if (contact_to!=NULL) {
        free_contacts(contact_to);
        contact_to=NULL;
    }
    if (contact_cc!=NULL) {
        free_contacts(contact_cc);
        contact_cc=NULL;
    }

    /* clear stylometric data */
    clear_style();
}

/* process a number of emails within a text file */
void process_emails(
    char * filename,
    contact * contact_list,
    struct tm start_date, struct tm end_date,
    int start_hour, int end_hour,
    int * month_histogram,
    int * year_histogram,
    int * day_histogram,
    int * week_histogram,
    char ** keywords,
    int gender,
    filename_list * corpus,
    int sentence_dump,
    int sentiment,
    FILE * fp_dates)
{
    FILE * fp=NULL;
    char * retval=NULL;
    char linestr[256];
    char buffer[10];
    int i=0;
    int line_number=0;
    int email_start_line_number=-1;
    int keywords_found=0;
    int email_processed=0;
    struct tm contact_date;

    int semantics_sentence_state=0,semantics_sentence_buffer_ctr=0;
    char semantics_sentence_buffer[256];

    int sentiment_sentence_state=0,sentiment_sentence_buffer_ctr=0;
    char sentiment_sentence_buffer[256];

    int semantics_from_to=0;
    int valence=0;
    int dummy=0;
    contact * cont;
#ifdef STYLOMETRICS
    int stylometrics_sentence_state=0,stylometrics_sentence_buffer_ctr=0;
    char stylometrics_sentence_buffer[256];
#endif

    i=0;

    /* clear the date fields */
    contact_date.tm_year=0;
    contact_date.tm_mon=0;
    contact_date.tm_mday=0;
    contact_date.tm_hour=0;
    contact_date.tm_min=0;

    linestr[0]='\0';
    buffer[0]='\0';

    if (strcmp(filename,"stdin")!=0) {
        fp = fopen(filename,"r");
    }
    else {
        fp = stdin;
    }
    if (fp != NULL) {
        while (!feof(fp)) {
            retval = fgets(linestr,255,fp);
            if (retval != NULL) {

                if (strlen(linestr)>6) {

                    /* look for colon character */
                    i=0;
                    while ((i<7) && (linestr[i]!=':')) {
                        buffer[i]=linestr[i];
                        i++;
                    }

                    /* end of the previous email */
                    if ((email_processed==0) &&
                            ((strncmp(buffer,FROM,i)==0) ||
                             (strncmp(buffer,TO,i)==0) ||
                             (strncmp(buffer,CC,i)==0) ||
                             (strncmp(buffer,DATE,i)==0))) {

                        process_email(
                            contact_list,contact_date,
                            start_date, end_date,start_hour, end_hour,
                            month_histogram,year_histogram,day_histogram,
                            week_histogram,keywords,gender,keywords_found,
                            valence,sentiment,fp_dates);

                        if (keywords_found != 0) {
                            corpus_update(filename,email_start_line_number,line_number,corpus);
                        }
                        email_start_line_number = line_number;
                        email_processed=1;
                        keywords_found=0;
                        valence=0;
                    }

                    /* check for keywords */
                    if ((keywords[0]!=NULL) && (keywords_found==0)) {
                        keywords_found = contains_keywords(linestr, keywords);
                    }

                    if (email_processed==0) {
#ifdef STYLOMETRICS
                        /* detect sentences and update stylometrics */
                        stylometrics_sentence_state = detect_sentence(
                                                          (char*)linestr,
                                                          stylometrics_sentence_state,
                                                          stylometrics_sentence_buffer, &stylometrics_sentence_buffer_ctr,&dummy,
                                                          stylometrics_process_sentence);
#endif
                    }

                    /* read From: To: and Cc: lines */
                    contact_from = read_from_line(linestr, buffer, i, gender, contact_from);
                    contact_to = read_to_line(linestr, buffer, i, gender, contact_to);
                    contact_cc = read_cc_line(linestr, buffer, i, gender, contact_cc);

                    /* read the date */
                    if (strncmp(buffer,DATE,i)==0) {
                        contact_date = read_email_date(linestr);
                    }

                    /* read the subject line */
                    if (strncmp(buffer,SUBJECT,i)==0) {
                        email_processed=0;
                        semantics_from_to=0;
                    }

                    if (email_processed==0) {
                        if ((contact_from!=NULL) && ((contact_to!=NULL) || (contact_cc!=NULL)) &&
                                ((keywords[0]==NULL) || (keywords_found!=0)) &&
                                ((start_date.tm_year==0) ||
                                 ((start_date.tm_year>0) && (end_date.tm_year==0) && (datedays(contact_date)>=datedays(start_date))) ||
                                 ((start_date.tm_year>0) && (end_date.tm_year>0) && (datedays(contact_date)>=datedays(start_date)) && (datedays(contact_date)<=datedays(end_date))))) {

                            if (sentiment!=0) {
                                /* sentiment analysis */
                                sentiment_sentence_state = detect_sentence(
                                                               (char*)linestr,
                                                               sentiment_sentence_state,
                                                               sentiment_sentence_buffer, &sentiment_sentence_buffer_ctr,&valence,
                                                               sentiment_process_sentence);
                            }

                            if (sentence_dump!=0) {
                                /* semantic analysis */

                                if (semantics_from_to==0) {
                                    /* Show the sensers and recipients */
                                    printf("\n%s",FROM);
                                    cont = contact_from;
                                    while (cont!=NULL) {
                                        printf("%s,%s,",cont->name,cont->email);
                                        cont = (contact*)(cont->next);
                                    }
                                    printf("\n%s",TO);
                                    cont = contact_to;
                                    while (cont!=NULL) {
                                        printf("%s,%s,",cont->name,cont->email);
                                        cont = (contact*)(cont->next);
                                    }
                                    printf("\n");
                                    if (contact_cc!=NULL) {
                                        printf("%s",CC);
                                        cont = contact_cc;
                                        while (cont!=NULL) {
                                            printf("%s,%s,",cont->name,cont->email);
                                            cont = (contact*)(cont->next);
                                        }
                                        printf("\n");
                                    }

                                    semantics_from_to=1;
                                }

                                semantics_sentence_state = detect_sentence(
                                                               (char*)linestr,
                                                               semantics_sentence_state,
                                                               semantics_sentence_buffer, &semantics_sentence_buffer_ctr,&dummy,
                                                               semantics_process_sentence);

                            }
                        }
                    }

                }

                line_number++;
            }
        }
#ifdef DEBUG
        printf("Close\n");
#endif
        fclose(fp);

        /* process the final email */
        process_email(
            contact_list,contact_date,
            start_date, end_date,start_hour, end_hour,
            month_histogram,year_histogram,day_histogram,
            week_histogram,keywords,gender,keywords_found,
            valence,sentiment,fp_dates);
    }
}
