/*
    Various string handling functions
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

int ContainsNonChars(char * ch)
{
    int i=0;
    for (i=0; i<strlen(ch); i++) {
        if ((ch[i]<32) || (ch[i]>126)) return 1;
    }
    return 0;
}

int ContainsWord(char * ch, char * search)
{
    int i=0,j=0;
    int len = strlen(ch)-strlen(search)-1;
    int len2 = strlen(search);
    for (i=0; i<len; i++) {
        for (j=0; j<len2; j++) {
            /* not case sensitive */
            if (toupper(ch[i+j])!=toupper(search[j])) break;
        }
        if (j==len2) {
            return 1;
        }
    }
    return 0;
}

int CountChars(char * ch, char search)
{
    int i=0,ctr=0;
    for (i=0; i<strlen(ch); i++) {
        if (ch[i]==search) ctr++;
    }
    return ctr;
}

int ContainsChar(char * ch, char search)
{
    int i=0;
    for (i=0; i<strlen(ch); i++) {
        if (ch[i]==search) return 1;
    }
    return 0;
}

void Trim(char * ch, char c)
{
    int start=0,end=strlen(ch)-1,i=0;
    while ((start<end) && (ch[start]==c)) start++;
    while ((end>0) && (ch[end]==c)) end--;

    if ((start==0) && (end==strlen(ch)-1)) {
        return;
    }
    else {
        if (start==0) {
            ch[end+1]='\0';
        }
        else {
            for (i=0; i<=end-start; i++) {
                ch[i]=ch[i+start];
            }
            ch[i]='\0';
        }
    }
}

int IsNumber(char c)
{
    if ((c>='0') && (c<='9')) {
        return 1;
    }
    return 0;
}

void TextOnly(char * ch, char * result)
{
    int i=0, n=0;

    for (i = 0; i < strlen(ch); i++) {
        if (((ch[i] >= 'a') &&
                (ch[i] <= 'z')) ||
                ((ch[i] >= 'A') &&
                 (ch[i] <= 'Z')) ||
                ((ch[i] >= '0') &&
                 (ch[i] <= '9')) ||
                (ch[i] == ' ')) {
            result[n++] = ch[i];
            if (n == STRING_BUFFER_MAX-2) break;
        }
    }
    result[n]='\0';
}

char * ToLower(char * ch)
{
    int i=0;
    for (i=0; i<strlen(ch); i++) {
        ch[i] = tolower(ch[i]);
    }
    return ch;
}

char * ToUpper(char * ch)
{
    int i=0;
    for (i=0; i<strlen(ch); i++) {
        ch[i] = toupper(ch[i]);
    }
    return ch;
}

void Split(char * ch, char separator, char ** result)
{
    int i=0,n=0;

    while ((i<strlen(ch)-1) && (ch[i]==separator)) i++;

    if (i<strlen(ch)-1) {
        result[n++]=&ch[i];

        while (i < strlen(ch)-1) {
            if ((i!=0) && (ch[i] == separator)) {
                if (ch[i-1] != separator) {
                    result[n++]=&ch[i+1];
                }
            }
            i++;
        }
    }
    result[n]=NULL;
}

int IndexOf(char ** words, char * word)
{
    int i=0,j=0;

    for (i=0; i<NUM_OF(words); i++) {
        for (j=0; j<strlen(words[i]); j++) {
            if ((j>=strlen(word)) || (word[j]=='\0') || (word[j]==' ') ||
                    (word[j]!=words[i][j])) break;
        }
        if (j==strlen(words[i])) {
            return i;
        }
    }
    return -1;
}

void RemoveCommonWords(
    char * text,
    char ** common_words,
    char * string_buffer,
    char ** list_buffer,
    char * result)
{
    int i=0,j=0,pos,n=0;

#ifdef DEBUG
    printf("RemoveCommonWords (before): %s\n", text);
#endif

    TextOnly(text,string_buffer);

#ifdef DEBUG
    printf("RemoveCommonWords (text only): %s\n", string_buffer);
#endif

    ToLower(string_buffer);
    Split(string_buffer,' ',list_buffer);

    i=0;
    while (list_buffer[i]!=NULL) {
        pos = IndexOf(common_words, list_buffer[i]);
        if (pos==-1) {
            if (n>0) result[n++]=' ';
            j=0;
            while (j<strlen(list_buffer[i])) {
                if ((list_buffer[i][j]==' ') || (list_buffer[i][j]=='\0')) break;
                result[n++] = list_buffer[i][j];
                j++;
            }
        }
        i++;
    }
    result[n]='\0';

#ifdef DEBUG
    printf("RemoveCommonWords (after): %s\n", result);
#endif
}

int contains_keywords(char * linestr, char ** keywords)
{
    int i=0,j=0;
    char buffer[256];

    if (keywords[0]!=NULL) {
        i=0;
        while (keywords[i]!=NULL) {
            /* transfer into a buffer */
            j=0;
            while ((j<strlen(keywords[i])) && (keywords[i][j]!=',')) {
                buffer[j]=keywords[i][j];
                j++;
            }
            buffer[j]='\0';
            /* compare */
            if (ContainsWord(linestr,buffer)!=0) {
                return 1;
            }
            i++;
        }
    }
    return 0;
}

int is_sentence(char * ch)
{
    int result=0;
    int i=0,ucase_ctr=0,nums=0;

    if (strlen(ch)>5) {
        if (CountChars(ch,' ')>1) {
            if (CountChars(ch,'-')<2) {
                if (ContainsChar(ch,':')==0) {
                    if (ContainsChar(ch,'&')==0) {
                        if (ContainsChar(ch,'@')==0) {
                            if (ContainsChar(ch,'/')==0) {
                                if (ContainsChar(ch,'=')==0) {
                                    if (ContainsChar(ch,'>')==0) {
                                        if (ContainsChar(ch,'#')==0) {
                                            if (CountChars(ch,';')<4) {
                                                if ((ContainsChar(ch,'[')==0) && (ContainsChar(ch,']')==0)) {
                                                    if (ContainsChar(ch,'_')==0) {
                                                        for (i=0; i<strlen(ch)-1; i++) {
                                                            if ((ch[i]==' ') && (ch[i+1]==' ')) return 0;
                                                            if ((ch[i]>='A') && (ch[i]<='Z')) {
                                                                ucase_ctr++;
                                                                if (ucase_ctr>5) return 0;
                                                            }
                                                            if ((ch[i]>='0') && (ch[i]<='9')) {
                                                                nums++;
                                                                if (nums>5) return 0;
                                                            }
                                                        }
                                                        result = 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return result;
}

int detect_words(
    char * sentence,
    char * buffer,
    int * value,
    void (*fn_process_word)(char*,int*))
{
    int i=0,n=0,ctr=0;
    for (i=0; i<strlen(sentence); i++) {
        if ((sentence[i]!=' ') && (sentence[i]!='.') && (sentence[i]!='?') && (sentence[i]!='!')) {
            buffer[n]=tolower(sentence[i]);
            if ((buffer[n]>='a') && (buffer[n]<='z')) n++;
        }
        else {
            buffer[n]='\0';
            n=0;
            (*fn_process_word)(buffer,value);
            ctr++;
        }
    }
    if (n>0) {
        buffer[n]='\0';
        (*fn_process_word)(buffer,value);
        ctr++;
    }
    return ctr;
}

/* Process the text and call a function when a sentence is detected */
int detect_sentence(
    char * text,
    int state,
    char * buffer, int * buffer_ctr,
    int * value,
    int (*fn_process_sentence)(char*,int*))
{
    int i=0,l=strlen(text);

    for (i=0; i<l; i++) {
        switch(state) {
        case 0: {
            if (i<l-1) {
                if ((text[i]>='A') && (text[i]<='Z') &&
                        (((text[i+1]>='a') && (text[i+1]<='z')) || (text[i+1]==' '))) {
                    state=1;
                }
            }
            break;
        }
        case 1: {
            if ((text[i]=='.') || (text[i]=='?') || (text[i]=='!')) {
                state=2;
            }
            break;
        }
        }
        if ((state>0) && (state<=2)) {
            if ((state==1) && ((text[i]==13) || (text[i]==10))) {
                buffer[*buffer_ctr] = ' ';
            }
            else {
                buffer[*buffer_ctr] = text[i];
            }
            if (*buffer_ctr<254) {
                *buffer_ctr=*buffer_ctr+1;
            }
            else {
                state = 0;
                *buffer_ctr=0;
            }
            if (state==2) {
                buffer[*buffer_ctr]='\0';
                state=0;
                *buffer_ctr=0;
                if (is_sentence(buffer)) {
                    (*fn_process_sentence)(buffer,value);
                }
            }
        }
    }

    return state;
}
