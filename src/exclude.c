/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

void exclude_read_file(char * filename, merge_rule * rules)
{
    FILE * fp;
    char linestr[256];
    merge_rule * new_rule;
    char * retval;
    int n;

    rules->next=NULL;
    rules->fromtext[0]='\0';
    rules->totext[0]='\0';

    fp = fopen(filename,"r");
    if (fp!=NULL) {
        while (!feof(fp)) {
            retval = fgets(linestr,255,fp);
            if (retval != NULL) {
                Trim(linestr,' ');
                /* comment */
                if (strlen(linestr)>0) {
                    if ((linestr[0]=='#') ||
                            (linestr[0]=='/')) {
                        continue;
                    }
                }
                if (strlen(linestr)>0) {

                    /* remove any end of line characters */
                    n=0;
                    while (n<strlen(linestr)) {
                        if ((linestr[n]==13) || (linestr[n]==10)) {
                            linestr[n]='\0';
                            break;
                        }
                        n++;
                    }

                    if (strlen(linestr)>0) {
                        if (rules->fromtext[0]=='\0') {
                            sprintf((char*)rules->fromtext,"%s",linestr);
                        }
                        else {
                            new_rule = (merge_rule*)malloc(sizeof(merge_rule));
                            if (new_rule!=NULL) {
                                sprintf((char*)new_rule->fromtext,"%s",linestr);
                                new_rule->next = (struct merge_rule*)(rules->next);
                                rules->next=(struct merge_rule*)new_rule;
                            }
                            else {
                                fprintf(stderr,"Unable to allocate memory for merge rule\n");
                            }
                        }
                    }
                }
            }
        }
        fclose(fp);
    }
}

void exclude_free(merge_rule * rules)
{
    merge_rule * next_rule = rules;
    merge_rule * tmp;

    while (next_rule!=NULL) {
        tmp = next_rule;
        next_rule=(merge_rule*)(next_rule->next);
        free((void*)tmp);
    }
}

void exclude_contacts_with_rules(contact * contact_list, merge_rule * rules)
{
    merge_rule * next_rule;
    contact * c;
    contact * example = (contact*)malloc(sizeof(contact));

    next_rule = rules;
    while (next_rule!=NULL) {
        if (next_rule->fromtext[0]!='\0') {
            sprintf(example->name,"%s",next_rule->fromtext);
            c = find_contact(example,contact_list);
            if (c!=NULL) {
                remove_contact(c);
            }
        }
        next_rule = (merge_rule*)(next_rule->next);
    }
    free(example);
}
