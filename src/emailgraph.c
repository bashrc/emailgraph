/*
  Email graphing
  Copyright (C) 2012 Bob Mottram
  bob@robotics.uk.to

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* Deallocating memory ----------------------------------------------------------------------*/

void free_filename_list(filename_list * filenames)
{
    filename_list * next_filename = filenames;
    filename_list * tmp;
    while (next_filename!=NULL) {
        tmp = next_filename;
        next_filename=(filename_list*)(next_filename->next);
        free(tmp);
    }
}

int stdin_has_data(int wait_sec)
{
    fd_set rfds;
    struct timeval tv;

    /* Watch stdin (fd 0) to see when it has input. */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    /* Wait */
    tv.tv_sec = wait_sec;
    tv.tv_usec = 0;

    /* does data exist within stdin? */
    return select(1, &rfds, NULL, NULL, &tv);
}

int main(int argc, char* argv[]) {
    int i,j,n,printcontacts=0,printdot=0,nonames=0,printtop=0,printinfluence=0,printvalence=0;
    int monthly=0,yearly=0,annual=0,daily=0,weekly=0,success;
    int gender=GENDER_UNKNOWN;
    char keywords_str[256];
    char title_str[256],plot_title[256];
    char * keywords[32];
    filename_list * corpus=NULL;
    merge_rule * rules=NULL;
    merge_rule * exclusions=NULL;
    char plot_dates[256];
    char plot_daily[256];
    char plot_weekly[256];
    char plot_monthly[256];
    char plot_annual[256];
    char plot_yearly[256];
    int min_interactions=-1;
    char merge_filename[256];
    char exclude_filename[256];
    char mailbox_filename[256];
    char mailbox_directory[256];
    struct tm start_date, end_date;
    int start_hour=-1, end_hour=-1;
    contact * contact_list = (contact*)malloc(sizeof(contact));
    int month_histogram[(MAX_YEAR-START_YEAR)*12];
    int year_histogram[MAX_YEAR-START_YEAR];
    int day_histogram[24];
    int week_histogram[24*7];
    int retval,linewidth=10;
    int sentence_dump=0;
    int sentiment=0;
    filename_list * filenames=NULL, * next_filename;
    int mnth;
    time_t current_time;
    int stdin_data_exists=0;
    FILE * fp_dates = 0;
    char * plot_dates_data = "temp_dates.txt";

    /* is there data to be read from stdin? */
    stdin_data_exists = stdin_has_data(1);

    /*
    	read_sentiment_file("sentiment.txt",1);
    	read_sentiment_file("sentiment.txt",0);
    	return 1;
    */

    if (contact_list==NULL) {
        fprintf(stderr,"main: unable to allocate memory\n");
        return(0);
    }

    init_contact(contact_list);

    /* clear histograms */
    for (i=0; i<MAX_YEAR-START_YEAR; i++) year_histogram[i]=0;
    for (i=0; i<(MAX_YEAR-START_YEAR)*12; i++) month_histogram[i]=0;
    for (i=0; i<24; i++) day_histogram[i]=0;
    for (i=0; i<24*7; i++) week_histogram[i]=0;

    title_str[0]='\0';
    keywords[0]=NULL;
    mailbox_filename[0]='\0';
    mailbox_directory[0]='\0';

    plot_dates[0]='\0';
    plot_daily[0]='\0';
    plot_weekly[0]='\0';
    plot_monthly[0]='\0';
    plot_annual[0]='\0';
    plot_yearly[0]='\0';

    memset((void*)&start_date,'\0',sizeof(struct tm));
    memset((void*)&end_date,'\0',sizeof(struct tm));
    start_date.tm_year=0;
    start_date.tm_mon=0;
    start_date.tm_mday=0;
    end_date.tm_year=0;
    end_date.tm_mon=0;
    end_date.tm_mday=0;

    clear_style();

    if (stdin_data_exists) {
        sprintf((char*)mailbox_filename,"%s","stdin");
    }

    for (i=1; i<argc; i++) {
        if ((strcmp(argv[i],"-m")==0) || (strcmp(argv[i],"--mbox")==0)) {
            /* read the mailbox filename */
            sprintf((char*)mailbox_filename,"%s",argv[i+1]);
            i++;
            continue;
        }
        if ((strcmp(argv[i],"-d")==0) || (strcmp(argv[i],"--dir")==0)) {
            /* read the directory */
            success=1;
            sprintf((char*)mailbox_directory,"%s",argv[i+1]);
            filenames = (filename_list*)malloc(sizeof(filename_list));
            if (filenames==NULL) {
                fprintf(stderr,"Unable to allocate memory for filenames\n");
                success=0;
            }
            else {
                filenames->next=NULL;
                retval = walk_dir(mailbox_directory, ".\\.$", WS_DOTFILES|WS_DEFAULT|WS_MATCHDIRS,filenames);
                switch(retval) {
                case WALK_BADIO: {
                    fprintf(stderr,"Directory '%s' Error %d: %s\n", mailbox_directory,errno,strerror(errno));
                    break;
                }
                case WALK_NAMETOOLONG: {
                    fprintf(stderr,"Directory name '%s' is too long\n", mailbox_directory);
                    break;
                }
                case WALK_BADPATTERN: {
                    fprintf(stderr,"Bad directory '%s'\n", mailbox_directory);
                    break;
                }
                }
                if ((retval==WALK_OK) && (filenames->next==NULL)) {
                    /* directory exists but contains no files */
                    fprintf(stderr,"No files found within the directory '%s'\n",mailbox_directory);
                    success=0;
                }
            }
            if ((retval!=WALK_OK) || (success==0)) {
                /* exit */
                free_filename_list(filenames);
                free(contact_list);
                if (corpus!=NULL) free(corpus);
                return 0;
            }
            i++;
            continue;
        }
        if ((strcmp(argv[i],"-v")==0) || (strcmp(argv[i],"--version")==0)) {
            printf("Version %.2f\n",VERSION);
            return 1;
        }
        if ((strcmp(argv[i],"-w")==0) || (strcmp(argv[i],"--linewidth")==0)) {
            linewidth=atoi(argv[i+1]);
            i++;
            continue;
        }
        if ((strcmp(argv[i],"-c")==0) || (strcmp(argv[i],"--contacts")==0)) {
            printcontacts=1;
        }
        if (strcmp(argv[i],"--valence")==0) {
            printvalence=1;
            sentiment=1;
        }
        if (strcmp(argv[i],"--sentiment")==0) {
            sentiment=1;
        }
        if ((strcmp(argv[i],"--sentimentnegative")==0) ||
                (strcmp(argv[i],"--negativesentiment")==0) ||
                (strcmp(argv[i],"--sentimentneg")==0)) {
            sentiment=-1;
        }
        if ((strcmp(argv[i],"-t")==0) || (strcmp(argv[i],"--top")==0)) {
            printtop=1;
        }
        if ((strcmp(argv[i],"-s")==0) || (strcmp(argv[i],"--sentences")==0) || (strcmp(argv[i],"--sentence")==0)) {
            sentence_dump=1;
        }
        if ((strcmp(argv[i],"-i")==0) || (strcmp(argv[i],"--inf")==0) || (strcmp(argv[i],"--influence")==0) || (strcmp(argv[i],"--influential")==0)) {
            printinfluence=1;
        }
        if (strcmp(argv[i],"--dot")==0) {
            printdot=1;
        }
        if ((strcmp(argv[i],"--test")==0) || (strcmp(argv[i],"--tests")==0)) {
            run_tests();
            return 1;
        }
        if (strcmp(argv[i],"--min")==0) {
            min_interactions=atoi(argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--merge")==0) {
            sprintf((char*)merge_filename,"%s", argv[i+1]);
            if (file_exists(merge_filename)==0) {
                fprintf(stderr,"Merge file '%s' Error: %d: %s\n",merge_filename,errno,strerror(errno));
                /* exit */
                free_filename_list(filenames);
                free(contact_list);
                if (corpus!=NULL) free(corpus);
                merge_free(rules);
                exclude_free(exclusions);
                return 0;
            }
            rules=(merge_rule*)malloc(sizeof(merge_rule));
            merge_read_file(merge_filename, rules);
            i++;
            continue;
        }
        if ((strcmp(argv[i],"--exclude")==0) || (strcmp(argv[i],"--remove")==0)) {
            sprintf((char*)exclude_filename,"%s", argv[i+1]);
            if (file_exists(exclude_filename)==0) {
                fprintf(stderr,"Exclude file '%s' Error: %d: %s\n",exclude_filename,errno,strerror(errno));
                /* exit */
                free_filename_list(filenames);
                free(contact_list);
                if (corpus!=NULL) free(corpus);
                merge_free(rules);
                exclude_free(exclusions);
                return 0;
            }
            exclusions=(merge_rule*)malloc(sizeof(merge_rule));
            exclude_read_file(exclude_filename, exclusions);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--gender")==0) {
            if (strlen(argv[i+1])==1) {
                if (toupper(argv[i+1][0])=='M') {
                    gender=GENDER_MALE;
                }
                if (toupper(argv[i+1][0])=='F') {
                    gender=GENDER_FEMALE;
                }
            }
            if (((strcmp(argv[i+1],"MF")==0) || (strcmp(argv[i+1],"FM")==0)) ||
                    ((strcmp(argv[i+1],"mf")==0) || (strcmp(argv[i+1],"fm")==0))) {
                gender=GENDER_MALE_FEMALE;
            }
            i++;
            continue;
        }
        if (strcmp(argv[i],"--nonames")==0) {
            nonames=1;
        }
        if (strcmp(argv[i],"--monthly")==0) {
            monthly=1;
        }
        if (strcmp(argv[i],"--yearly")==0) {
            yearly=1;
        }
        if (strcmp(argv[i],"--annual")==0) {
            annual=1;
        }
        if (strcmp(argv[i],"--daily")==0) {
            daily=1;
        }
        if (strcmp(argv[i],"--weekly")==0) {
            weekly=1;
        }
        if ((strcmp(argv[i],"-h")==0) || (strcmp(argv[i],"-?")==0) || (strcmp(argv[i],"--help")==0)) {
            print_help();
            return(1);
        }
        if (strcmp(argv[i],"--start")==0) {
            time(&current_time);

            if ((strcmp(argv[i+1],"month")==0) || (strcmp(argv[i+1],"Month")==0)) {
                start_date=*localtime(&current_time);
                mnth = start_date.tm_mon-1;
                if (mnth<0) {
                    start_date.tm_mon=11;
                    start_date.tm_year--;
                }
                else {
                    start_date.tm_mon=mnth;
                }
                start_date.tm_year+=1900;
            }
            else {
                if ((strcmp(argv[i+1],"6month")==0) || (strcmp(argv[i+1],"6Month")==0) ||
                        (strcmp(argv[i+1],"6months")==0) || (strcmp(argv[i+1],"6Months")==0)) {
                    start_date=*localtime(&current_time);
                    mnth = start_date.tm_mon-6;
                    if (mnth<0) {
                        start_date.tm_mon=(12+mnth);
                        start_date.tm_year--;
                    }
                    else {
                        start_date.tm_mon=mnth;
                    }
                    start_date.tm_year+=1900;
                }
                else {

                    if ((strcmp(argv[i+1],"3month")==0) || (strcmp(argv[i+1],"3Month")==0) ||
                            (strcmp(argv[i+1],"3months")==0) || (strcmp(argv[i+1],"3Months")==0)) {
                        start_date=*localtime(&current_time);
                        mnth = start_date.tm_mon-3;
                        if (mnth<0) {
                            start_date.tm_mon=(12+mnth);
                            start_date.tm_year--;
                        }
                        else {
                            start_date.tm_mon=mnth;
                        }
                        start_date.tm_year+=1900;
                    }
                    else {
                        if ((strcmp(argv[i+1],"year")==0) || (strcmp(argv[i+1],"Year")==0) || (strcmp(argv[i+1],"Yr")==0)) {
                            start_date=*localtime(&current_time);
                            start_date.tm_year+=1899;
                        }
                        else {
                            if ((strcmp(argv[i+1],"2year")==0) || (strcmp(argv[i+1],"2Year")==0) || (strcmp(argv[i+1],"2Yr")==0)) {
                                start_date=*localtime(&current_time);
                                start_date.tm_year+=1898;
                            }
                            else {
                                if ((strcmp(argv[i+1],"3year")==0) || (strcmp(argv[i+1],"3Year")==0) || (strcmp(argv[i+1],"3Yr")==0)) {
                                    start_date=*localtime(&current_time);
                                    start_date.tm_year+=1897;
                                }
                                else {
                                    if ((strcmp(argv[i+1],"4year")==0) || (strcmp(argv[i+1],"4Year")==0) || (strcmp(argv[i+1],"4Yr")==0)) {
                                        start_date=*localtime(&current_time);
                                        start_date.tm_year+=1896;
                                    }
                                    else {
                                        if ((strcmp(argv[i+1],"5year")==0) || (strcmp(argv[i+1],"5Year")==0) || (strcmp(argv[i+1],"5Yr")==0)) {
                                            start_date=*localtime(&current_time);
                                            start_date.tm_year+=1895;
                                        }
                                        else {
                                            start_date = parse_date(argv[i+1]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            i++;
            continue;
        }
        if (strcmp(argv[i],"--plotdensity")==0) {
            sprintf((char*)plot_dates,"%s",argv[i+1]);
            fp_dates = fopen(plot_dates_data,"w");
            i++;
            continue;
        }
        if (strcmp(argv[i],"--end")==0) {
            end_date = parse_date(argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--starthour")==0) {
            start_hour = atoi(argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--endhour")==0) {
            end_hour = atoi(argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--plotdaily")==0) {
            sprintf((char*)plot_daily,"%s",argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--plotweekly")==0) {
            sprintf((char*)plot_weekly,"%s",argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--plotmonthly")==0) {
            sprintf((char*)plot_monthly,"%s",argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--plotannual")==0) {
            sprintf((char*)plot_annual,"%s",argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--corpus")==0) {
            if (corpus==NULL) {
                corpus = (filename_list*)malloc(sizeof(filename_list));
                if (corpus!=NULL) {
                    corpus->next=NULL;
                }
            }
        }
        if (strcmp(argv[i],"--plotyearly")==0) {
            sprintf((char*)plot_yearly,"%s",argv[i+1]);
            i++;
            continue;
        }
        if (strcmp(argv[i],"--title")==0) {
            sprintf((char*)title_str,"%s",argv[i+1]);
            i++;
            n=0;
            for (j=0; j<strlen(title_str); j++) {
                if (title_str[j]!=(char)34) title_str[n++]=title_str[j];
            }
            title_str[n]='\0';
            continue;
        }
        if (strcmp(argv[i],"--keywords")==0) {
            sprintf((char*)keywords_str,"%s",argv[i+1]);
            i++;
            if (keywords_str[0]==(char)34) {
                n=0;
                for (j=0; j<strlen(keywords_str); j++) {
                    if (keywords_str[j]!=(char)34) keywords_str[n++]=keywords_str[j];
                }
                keywords_str[n]='\0';
            }
            Split(keywords_str,',',keywords);
            continue;
        }
    }

    if ((mailbox_filename[0]=='\0') && (mailbox_directory[0]=='\0')) {
        fprintf(stderr,"No mailbox file or directory was specified\n");
        /* exit */
        free_filename_list(filenames);
        free(contact_list);
        if (corpus!=NULL) free(corpus);
        merge_free(rules);
        exclude_free(exclusions);
        return 0;
    }

    if ((corpus!=NULL) && (keywords[0]==NULL)) {
        free(corpus);
        corpus=NULL;
    }

    if (mailbox_filename[0]!='\0') {
        if ((!stdin_data_exists) && (file_exists(mailbox_filename)==0)) {
            /* No mailbox file was found */
            fprintf(stderr,"Mailbox file '%s' Error %d: %s\n", mailbox_filename,errno,strerror(errno));
            /* exit */
            free(contact_list);
            if (corpus!=NULL) free(corpus);
            merge_free(rules);
            exclude_free(exclusions);
            return 0;
        }
        process_emails(
            (char*)mailbox_filename, contact_list, start_date,
            end_date, start_hour, end_hour,
            (int*)month_histogram, (int*)year_histogram,
            (int*)day_histogram, (int*)week_histogram, keywords, gender,corpus,sentence_dump,sentiment,
            fp_dates);
    }
    else {
        if (filenames!=NULL) {
            next_filename = (filename_list*)(filenames->next);
            while (next_filename!=NULL) {
                process_emails(
                    (char*)(next_filename->filename), contact_list,
                    start_date, end_date, start_hour, end_hour,
                    (int*)month_histogram, (int*)year_histogram,
                    (int*)day_histogram, (int*)week_histogram, keywords, gender,corpus,sentence_dump,sentiment,
                    fp_dates);
                next_filename = (filename_list*)(next_filename->next);
            }
        }
    }

    if (fp_dates!=NULL) fclose(fp_dates);

    free_filename_list(filenames);

    /* extract corpus of text */
    if (corpus!=NULL) {
        corpus_extract(corpus);
    }
    corpus_free(corpus);

    contact_list = merge_duplicates(contact_list);

    contact_list = remove_non_names(contact_list);

    consolidate_links(contact_list);

    merge_contacts_with_rules(contact_list, rules);

    exclude_contacts_with_rules(contact_list, exclusions);

    update_influence(contact_list);

    update_valence(contact_list);

    if (corpus==NULL) {
        if (contacts_size(contact_list)>1) {

            if (printinfluence==1) {
                contact_list = print_influence(contact_list,nonames,min_interactions);
            }
            else {
                if (printvalence==1) {
                    print_valence(contact_list,nonames,min_interactions);
                }
                else {
                    if (printtop==1) {
                        contact_list = print_top(contact_list,nonames,min_interactions);
                    }
                    else {
                        if (printcontacts==1) {
                            contact_list = sort_contacts(contact_list,SORT_NAMES);
                            print_contacts(contact_list);
                        }
                        else {
                            if (printdot==1) {
                                print_dot(contact_list,nonames,min_interactions,linewidth,sentiment);
                            }
                            else {
                                if (monthly==1) {
                                    print_volume_monthly((int*)month_histogram);
                                }
                                else {
                                    if (yearly==1) {
                                        print_volume_yearly((int*)year_histogram);
                                    }
                                    else {
                                        if (annual==1) {
                                            print_volume_annual_average((int*)month_histogram);
                                        }
                                        else {
                                            if (daily==1) {
                                                print_volume_daily((int*)day_histogram);
                                            }
                                            else {
                                                if (weekly==1) {
                                                    print_volume_weekly((int*)week_histogram);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (plot_dates[0]!='\0') {
            if (title_str[0]=='\0') {
                sprintf((char*)plot_title,"%s","Email Density Plot");
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_email_dates(
                (char*)plot_dates,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                plot_dates_data);
        }

        if (plot_daily[0]!='\0') {
            if (title_str[0]=='\0') {
                if (sentiment==0) {
                    sprintf((char*)plot_title,"%s","Total Daily Emails");
                }
                else {
                    if (sentiment>0) {
                        sprintf((char*)plot_title,"%s","Total Daily Email Valence");
                    }
                    else {
                        sprintf((char*)plot_title,"%s","Total Daily Negative Email Valence");
                    }
                }
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_volume_daily(
                (char*)plot_daily,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                day_histogram,
                sentiment);
        }
        if (plot_weekly[0]!='\0') {
            if (title_str[0]=='\0') {
                if (sentiment==0) {
                    sprintf((char*)plot_title,"%s","Weekly Emails");
                }
                else {
                    if (sentiment>0) {
                        sprintf((char*)plot_title,"%s","Weekly Email Valence");
                    }
                    else {
                        sprintf((char*)plot_title,"%s","Weekly Negative Email Valence");
                    }
                }
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_volume_weekly(
                (char*)plot_weekly,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                week_histogram,
                sentiment);
        }
        if (plot_monthly[0]!='\0') {
            if (title_str[0]=='\0') {
                if (sentiment==0) {
                    sprintf((char*)plot_title,"%s","Monthly Emails");
                }
                else {
                    if (sentiment>0) {
                        sprintf((char*)plot_title,"%s","Monthly Email Valence");
                    }
                    else {
                        sprintf((char*)plot_title,"%s","Monthly Negative Email Valence");
                    }
                }
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_volume_monthly(
                (char*)plot_monthly,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                month_histogram,
                sentiment);
        }
        if (plot_annual[0]!='\0') {
            if (title_str[0]=='\0') {
                if (sentiment==0) {
                    sprintf((char*)plot_title,"%s","Average Annual Emails");
                }
                else {
                    if (sentiment>0) {
                        sprintf((char*)plot_title,"%s","Average Annual Email Valence");
                    }
                    else {
                        sprintf((char*)plot_title,"%s","Average Annual Negative Email Valence");
                    }
                }
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_volume_annual_average(
                (char*)plot_annual,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                month_histogram,
                sentiment);
        }
        if (plot_yearly[0]!='\0') {
            if (title_str[0]=='\0') {
                if (sentiment==0) {
                    sprintf((char*)plot_title,"%s","Total Yearly Emails");
                }
                else {
                    if (sentiment>0) {
                        sprintf((char*)plot_title,"%s","Total Yearly Email Valence");
                    }
                    else {
                        sprintf((char*)plot_title,"%s","Total Yearly Negative Email Valence");
                    }
                }
            }
            else {
                sprintf((char*)plot_title,"%s",title_str);
            }
            plot_volume_yearly(
                (char*)plot_yearly,
                (char*)plot_title,NULL,
                IMAGE_WIDTH,IMAGE_HEIGHT,
                year_histogram,
                sentiment);
        }

        if ((printcontacts==0) && (printdot==0) && (sentence_dump==0) &&
                (monthly==0) && (yearly==0) && (annual==0) && (daily==0) && (weekly==0) &&
                (plot_daily[0]=='\0') && (plot_weekly[0]=='\0') && (plot_monthly[0]=='\0') &&
                (plot_yearly[0]=='\0') && (printtop==0) && (printvalence==0)) {
            printf("%d Contacts detected\n", contacts_size(contact_list));
        }

        free_contacts(contact_list);
        contact_list=NULL;
        merge_free(rules);
        exclude_free(exclusions);
    }
    else {
        printf("No results found\n");
        free_contacts(contact_list);
        contact_list=NULL;
        merge_free(rules);
        exclude_free(exclusions);
        return 0;
    }

#ifdef DEBUG
    printf("Ended Successfully\n");
#endif
    return(1);
}
