#include "emailgraph.h"

int test_init_contact()
{
    int tests=0;
    contact * contact_list = (contact*)malloc(sizeof(contact));

    printf("test_init_contact...");

    assert(contact_list!=NULL);
    tests++;

    init_contact(contact_list);
    assert(contact_list->ID==0);
    tests++;
    assert(contact_list->hash==0);
    tests++;
    assert(contact_list->selected==0);
    tests++;
    assert(contact_list->gender==0);
    tests++;
    assert(contact_list->name[0]=='\0');
    tests++;
    assert(contact_list->email[0]=='\0');
    tests++;
    assert(contact_list->sent==0);
    tests++;
    assert(contact_list->received==0);
    tests++;
    assert(contact_list->influence==0);
    tests++;
    assert(contact_list->next==NULL);
    tests++;
    assert(contact_list->previous==NULL);
    tests++;
    assert(contact_list->links==NULL);
    tests++;
    assert(contact_list->events==NULL);
    tests++;

    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_create_contacts()
{
    int i,tests=0;
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * c = (contact*)malloc(sizeof(contact));
    char * nyms[] = {
        "Dave","dave@sillynyms.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@mock.net",
        "Tich", "tich@gigantictich.net"
    };

    printf("test_create_contacts...");

    assert(contact_list!=NULL);
    tests++;

    init_contact(contact_list);

    for (i=0; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_list,ENTRY_FROM);
    }

    assert(contacts_size(contact_list)==6);
    tests++;

    free(c);
    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_links()
{
    int i,tests=0;
    contact * fromcontacts[CONTACT_BUFFER_SIZE];
    contact * tocontacts[CONTACT_BUFFER_SIZE];
    contact * cccontacts[CONTACT_BUFFER_SIZE];
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * contact_from = (contact*)malloc(sizeof(contact));
    contact * contact_to = (contact*)malloc(sizeof(contact));
    contact * c = (contact*)malloc(sizeof(contact));
    char * nyms[] = {
        "Dave","dave@sillynyms.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@mock.net",
        "Tich", "tich@gigantictich.net"
    };

    printf("test_links...");

    fromcontacts[0]=NULL;
    tocontacts[0]=NULL;
    cccontacts[0]=NULL;

    init_contact(contact_from);
    init_contact(contact_to);
    init_contact(contact_list);

    assert(contact_from!=NULL);
    tests++;
    assert(contact_to!=NULL);
    tests++;

    for (i=0; i<3; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_from,ENTRY_FROM);
        assert(contacts_contains(c,contact_from)==0);
        tests++;
        assert(contacts_contains(contact_from,contact_list)==0);
        tests++;
    }
    for (i=3; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_to,ENTRY_TO);
        assert(contacts_contains(c,contact_to)==0);
        tests++;
        assert(contacts_contains(contact_to,contact_list)==0);
        tests++;
    }

    assert(contacts_size(contact_to)==3);
    tests++;
    assert(contacts_size(contact_from)==3);
    tests++;

    add_contacts(contact_to,contact_list,ENTRY_TO,tocontacts,0);
    add_contacts(contact_from,contact_list,ENTRY_FROM,fromcontacts,0);

    assert(contacts_contains(contact_from,contact_list)==0);
    tests++;

    assert(contacts_contains(contact_to,contact_list)==0);
    tests++;

    assert(add_contact_links(fromcontacts,tocontacts,cccontacts,GENDER_UNKNOWN)!=0);
    tests++;

    free(c);
    free_contacts(contact_from);
    free_contacts(contact_to);
    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_remove_contact()
{
    int i,tests=0;
    contact * fromcontacts[CONTACT_BUFFER_SIZE];
    contact * tocontacts[CONTACT_BUFFER_SIZE];
    contact * cccontacts[CONTACT_BUFFER_SIZE];
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * contact_from = (contact*)malloc(sizeof(contact));
    contact * contact_to = (contact*)malloc(sizeof(contact));
    contact * c = (contact*)malloc(sizeof(contact));
    contact * victim=NULL;
    char * nyms[] = {
        "Dave","dave@sillynyms.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@mock.net",
        "Tich", "tich@gigantictich.net"
    };
    char teststr1[CONTACT_NAME_SIZE] = "Mick";

    printf("test_remove_contact...");

    fromcontacts[0]=NULL;
    tocontacts[0]=NULL;
    cccontacts[0]=NULL;

    init_contact(contact_from);
    init_contact(contact_to);
    init_contact(contact_list);

    for (i=0; i<3; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_from,ENTRY_FROM);
    }
    for (i=3; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_to,ENTRY_TO);
    }

    add_contacts(contact_to,contact_list,ENTRY_TO,tocontacts,0);
    add_contacts(contact_from,contact_list,ENTRY_FROM,fromcontacts,0);

    add_contact_links(fromcontacts,tocontacts,cccontacts,GENDER_UNKNOWN);

    init_contact(c);
    sprintf((char*)c->name,"%s",teststr1);
    victim = find_contact(c,contact_list);
    assert(victim!=NULL);
    tests++;
    assert(strcmp((char*)(victim->name),teststr1)==0);
    tests++;

    assert(contacts_size(contact_list)==6);
    tests++;
    remove_contact(victim);
    assert(contacts_size(contact_list)==5);
    tests++;

    free(c);
    free_contacts(contact_from);
    free_contacts(contact_to);
    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_influence()
{
    int i,tests=0;
    contact * fromcontacts[CONTACT_BUFFER_SIZE];
    contact * tocontacts[CONTACT_BUFFER_SIZE];
    contact * cccontacts[CONTACT_BUFFER_SIZE];
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * contact_from = (contact*)malloc(sizeof(contact));
    contact * contact_to = (contact*)malloc(sizeof(contact));
    contact * c = (contact*)malloc(sizeof(contact));
    contact * c2=NULL;
    char * nyms[] = {
        "Dave","dave@sillynyms.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@mock.net",
        "Tich", "tich@gigantictich.net"
    };

    printf("test_influence...");

    fromcontacts[0]=NULL;
    tocontacts[0]=NULL;
    cccontacts[0]=NULL;

    init_contact(contact_from);
    init_contact(contact_to);
    init_contact(contact_list);

    for (i=0; i<3; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_from,ENTRY_FROM);
    }
    for (i=3; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_to,ENTRY_TO);
    }

    add_contacts(contact_to,contact_list,ENTRY_TO,tocontacts,0);
    add_contacts(contact_from,contact_list,ENTRY_FROM,fromcontacts,0);

    add_contact_links(fromcontacts,tocontacts,cccontacts,GENDER_UNKNOWN);

    update_influence(contact_list);

    for (i=0; i<3; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        c2 = find_contact(c,contact_list);
        assert(c2!=NULL);
        tests++;
        assert(c2->influence>0);
        tests++;
    }

    for (i=3; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        c2 = find_contact(c,contact_list);
        assert(c2!=NULL);
        tests++;
        assert(c2->influence==0);
        tests++;
    }

    free(c);
    free_contacts(contact_from);
    free_contacts(contact_to);
    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_merge(int index1, int index2)
{
    int i,tests=0;
    contact * fromcontacts[CONTACT_BUFFER_SIZE];
    contact * tocontacts[CONTACT_BUFFER_SIZE];
    contact * cccontacts[CONTACT_BUFFER_SIZE];
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * contacts_all = (contact*)malloc(sizeof(contact));
    contact * c = (contact*)malloc(sizeof(contact));
    contact * c2=NULL, * c3=NULL;
    char * nyms[] = {
        "Dave","dave@sillynyms.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@sillynyms.biz",
        "Tich", "tich@gigantictich.net"
    };

    printf("test_merge %d <- %d...", index1, index2);

    fromcontacts[0]=NULL;
    tocontacts[0]=NULL;
    cccontacts[0]=NULL;

    init_contact(contact_list);
    init_contact(contacts_all);

    for (i=0; i<3; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contacts_all,ENTRY_FROM);
    }
    for (i=3; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contacts_all,ENTRY_TO);
    }

    add_contacts(contacts_all,contact_list,ENTRY_FROM,fromcontacts,0);
    add_contacts(contacts_all,contact_list,ENTRY_TO,tocontacts,0);

    /* full connectivity */
    add_contact_links(fromcontacts,tocontacts,cccontacts,GENDER_UNKNOWN);

    update_IDs(contact_list);

    for (i=0; i<6; i++) {
        c2 = get_contact(contact_list,i);
        if (links_size(c2)!=(6-1)*2) {
            printf("\nLinks1 %d = %d\n", i, links_size(c2));
        }
        assert(links_size(c2)==(6-1)*2);
        tests++;
    }

    c2 = get_contact(contact_list,index1);
    assert(c2!=NULL);
    tests++;

    c3 = get_contact(contact_list,index2);
    assert(c3!=NULL);
    tests++;

    c2->sent=1;
    c3->sent=3;

    assert(contacts_size(contact_list)==6);
    tests++;

    copy_contact(c,c3);

    contact_list = merge_contacts(c2,c3,contact_list);

    c3 = find_contact(c,contact_list);
    assert(c3==NULL);
    tests++;

    assert(c2->sent==4);
    tests++;

    assert(contacts_size(contact_list)==5);
    tests++;

    for (i=0; i<5; i++) {
        c2 = get_contact(contact_list,i);
        if (c2->ID!=index1) {
            if (links_size(c2)!=(6-2)*2) {
                printf("\nLinks2 %d = %d\n", i, links_size(c2));
            }
            assert(links_size(c2)==(6-2)*2);
        }
        else {
            if (links_size(c2)!=(6-2)*2+1) {
                printf("\nLinks2 %d = %d\n", i, links_size(c2));
            }
            assert(links_size(c2)==(6-2)*2+1);
        }
        tests++;
    }

    free(c);
    free_contacts(contacts_all);
    free_contacts(contact_list);
    printf("Ok\n");
    return tests;
}

int test_merge_contacts()
{
    int i,j;
    int tests=0;

    for (i=0; i<6; i++) {
        for (j=0; j<6; j++) {
            if (i!=j) {
                tests+=test_merge(i,j);
            }
        }
    }
    return tests;
}

int test_read_emails()
{
    int tests=0;

    char * email1[] = {
        "From: Alice Test <alice@testdomain.net>",
        "To: Bob Test <bob@testdomain.net>",
        "Date: 8/25/2010 10:30",
        "Subject: Test message",
        "This is a test message"
    };

    char * email2[] = {
        "From: Bob Test <bob@testdomain.net>",
        "To: OtherGuy <other@testdomain.net>",
        "Date: 7/20/2010 14:12",
        "Subject: Cat pictures",
        "This is a another test message"
    };

    char * email3[] = {
        "From: OtherGuy <other@testdomain.net>",
        "To: Alice Test <alice@testdomain.net>",
        "Date: 9/25/2010 17:37",
        "Subject: Boring stuff",
        "Blah blah blah"
    };

    char * email4[] = {
        "From: Bob Test <bob@testdomain.net>",
        "To: Alice Test <alice@testdomain.net>",
        "Date: 10/3/2010 9:42",
        "Subject: About your email",
        "I hated it"
    };

    char * email5[] = {
        "From: bob@testdomain.net",
        "To: alice@testdomain.net",
        "Date: 11/15/2010 7:02",
        "Subject: No wait...",
        "I haza plan.  It's a long shot, but it just might work."
    };

    char * email6[] = {
        "From: na <invalid@testdomain.net>",
        "To: other@testdomain.net",
        "Date: 5/30/2010 11:14",
        "Subject: All matters pertaining to civic statuary",
        "Municipal pride expressed in brick and concrete."
    };

    int i;
    FILE * fp;
    char filename[100];
    contact * contact_list, * user;
    contact * c = (contact*)malloc(sizeof(contact));
    struct tm start_date;
    struct tm end_date;
    int month_histogram[(MAX_YEAR-START_YEAR)*12];
    int year_histogram[MAX_YEAR-START_YEAR];
    int day_histogram[24];
    int week_histogram[24*7];
    char * keywords[32];

    printf("test_read_emails...");

    contact_list = (contact*)malloc(sizeof(contact));
    init_contact(contact_list);
    keywords[0]=NULL;
    start_date.tm_year=0;
    end_date.tm_year=0;

    /* clear histograms */
    for (i=0; i<MAX_YEAR-START_YEAR; i++) year_histogram[i]=0;
    for (i=0; i<(MAX_YEAR-START_YEAR)*12; i++) month_histogram[i]=0;
    for (i=0; i<24; i++) day_histogram[i]=0;
    for (i=0; i<24*7; i++) week_histogram[i]=0;

    clear_style();

    sprintf((char*)filename,"%s","test_read_emails.test");

    fp = fopen(filename,"w");
    assert (fp!=NULL);
    tests++;

    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email1[i]);
    }
    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email2[i]);
    }
    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email3[i]);
    }
    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email4[i]);
    }
    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email5[i]);
    }
    for (i=0; i<5; i++) {
        fprintf(fp,"%s\n",email6[i]);
    }
    fclose(fp);

    process_emails(
        (char*)filename,
        contact_list,
        start_date, end_date,
        -1, -1,
        month_histogram,
        year_histogram,
        day_histogram,
        week_histogram,
        keywords,
        GENDER_UNKNOWN,
        NULL,0,0,0);

    if (contacts_size(contact_list)!=5) {
        printf("\n");
        print_contacts(contact_list);
        printf("Contacts: %d\n", contacts_size(contact_list));
    }

    assert(contacts_size(contact_list)==5);
    tests++;

    contact_list = merge_duplicates(contact_list);

    contact_list = remove_non_names(contact_list);

    consolidate_links(contact_list);

    if (contacts_size(contact_list)!=3) {
        printf("\n");
        print_contacts(contact_list);
        printf("Contacts: %d\n", contacts_size(contact_list));
    }

    assert(contacts_size(contact_list)==3);
    tests++;

    init_contact(c);
    sprintf((char*)c->name,"%s","Alice Test");
    user = find_contact(c,contact_list);
    assert(user!=NULL);
    tests++;

    assert(user->received==3);
    tests++;
    assert(user->sent==1);
    tests++;

    free_contacts(contact_list);
    free(c);

    printf("Ok\n");
    return tests;
}

int test_sort_contacts()
{
    int i,tests=0;
    contact * contact_list = (contact*)malloc(sizeof(contact));
    contact * contact_list2 = contact_list;
    contact * c = (contact*)malloc(sizeof(contact));
    char * nyms[] = {
        "Zoom","zoom@testuser.net",
        "Dee", "dee@sillynyms.biz",
        "Dozy", "dozy@totallybogus.com",
        "Beaky", "beaky@dodgygeezers.org",
        "Mick", "mick@mock.net",
        "Tich", "tich@gigantictich.net"
    };
    char teststr1[CONTACT_NAME_SIZE] = "Beaky";

    printf("test_sort_contacts...");

    assert(contact_list!=NULL);
    tests++;

    init_contact(contact_list);

    for (i=0; i<6; i++) {
        init_contact(c);
        sprintf((char*)c->name,"%s",nyms[i*2]);
        sprintf((char*)c->email,"%s",nyms[i*2+1]);
        add_contact(c,contact_list,ENTRY_FROM);
    }

    assert(contacts_size(contact_list)==6);
    tests++;

    contact_list2 = sort_contacts(contact_list,SORT_NAMES);

    if (contact_list==contact_list2) {
        printf("\n\n");
        print_contacts(contact_list2);
    }

    assert(contact_list!=contact_list2);
    tests++;

    if (contacts_size(contact_list2)!=6) {
        print_contacts(contact_list2);
    }
    assert(contacts_size(contact_list2)==6);
    tests++;

    if (strcmp(contact_list2->name,teststr1)!=0) {
        printf("\n");
        print_contacts(contact_list2);
    }
    assert(strcmp(contact_list2->name,teststr1)==0);
    tests++;

    free(c);
    free_contacts(contact_list2);
    printf("Ok\n");
    return tests;
}

void run_tests()
{
    int tests=0;
    tests += test_init_contact();
    tests += test_create_contacts();
    tests += test_links();
    tests += test_remove_contact();
    tests += test_influence();
    tests += test_merge_contacts();
    tests += test_read_emails();
    tests += test_sort_contacts();
    printf("%d tests completed\n",tests);
}
