/*
    Email graphing
    Copyright (C) 2011 Bob Mottram
    bob@robotics.uk.to

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* returns the number of links for the given contact */
int links_size(contact * c)
{
    int links=0;
    contact_link * next_link = (contact_link*)(c->links);
    while (next_link!=NULL) {
        links++;
        next_link = (contact_link*)(next_link->next);
    }
    return links;
}

/* returns the number of contacts in the list */
int contacts_size(contact * contact_list)
{
    int i=0;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        i++;
        next_contact = (contact*)(next_contact->next);
    }
    return i;
}

/* returns the maximum number of hits on a contact link */
unsigned int max_contact_link_hits(contact * contact_list)
{
    unsigned int max=0;
    contact * next_contact = contact_list;
    contact_link * next_link;
    while (next_contact!=NULL) {
        next_link = (contact_link*)next_contact->links;
        while (next_link!=NULL) {
            if (next_link->hits>max) max=next_link->hits;
            next_link = (contact_link*)next_link->next;
        }
        next_contact = (contact*)(next_contact->next);
    }
    return max;
}

/* returns the maximum influence value */
unsigned int max_influence(contact * contact_list)
{
    unsigned int max=0;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if (next_contact->influence>max) max = next_contact->influence;
        next_contact = (contact*)(next_contact->next);
    }
    return max;
}

/* returns the maximum valence value for a contact */
int max_valence(contact * contact_list)
{
    int max=0;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if (next_contact->valence>max) max = next_contact->valence;
        next_contact = (contact*)(next_contact->next);
    }
    return max;
}

/* returns the maximum valence value for a contact */
int min_valence(contact * contact_list)
{
    int min=0;
    contact * next_contact = contact_list;
    while (next_contact!=NULL) {
        if (next_contact->valence<min) min = next_contact->valence;
        next_contact = (contact*)(next_contact->next);
    }
    return min;
}
