/*
  Email graphing
  Copyright (C) 2011 Bob Mottram
  bob@robotics.uk.to

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "emailgraph.h"

/* is the given date valid? */
int valid_date(struct tm date)
{
    if ((date.tm_year>1900) && (date.tm_year<MAX_YEAR) &&
            (date.tm_mon>0) && (date.tm_mon<=12) &&
            (date.tm_mday>0) && (date.tm_mday<=31) &&
            (date.tm_hour>=0) && (date.tm_hour<24) &&
            (date.tm_min>=0) && (date.tm_min<60)) {
        return 1;
    }
    return 0;
}

/* Returns a number corresponding to the day of the week */
int day_of_week(int month_day, int month, int year)
{
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    year -= month < 3;
    return (year + year/4 - year/100 + year/400 + t[month-1] + month_day) % 7;
}

unsigned int datedays(struct tm date)
{
    return date.tm_mday+(date.tm_mon*31)+(date.tm_year*31*12);
}

/* Parses a date of the type dd/mm/yyyy or dd-mm-yyyy */
struct tm parse_date(char * date_str)
{
    struct tm result;
    char * data[3], numstr[5];
    int i,j,n;

    result.tm_year=0;
    result.tm_mday=0;
    result.tm_mon=0;
    memset((void*)&result,'\0',sizeof(struct tm));

    if (ContainsChar(date_str,'/')!=0) {
        Split(date_str,'/',data);
    }
    if (ContainsChar(date_str,'-')!=0) {
        Split(date_str,'-',data);
    }
    if (ContainsChar(date_str,'.')!=0) {
        Split(date_str,'.',data);
    }

    j=0;
    for (i=0; i<3; i++) {
        n=0;
        while ((j<strlen(date_str)) &&
                (date_str[j]!='/') &&
                (date_str[j]!='-') &&
                (date_str[j]!='.')) {
            numstr[n++]=date_str[j];
            j++;
        }
        numstr[n]='\0';
        switch(i) {
        case 0: {
            result.tm_mday = atoi(numstr);
            break;
        }
        case 1: {
            result.tm_mon = atoi(numstr);
            break;
        }
        case 2: {
            result.tm_year = atoi(numstr);
            break;
        }
        }
        j++;
    }

    return result;
}
